﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class UpdateCurrentEffortViewModel
    {
        public int Id { get; set; }

        public string EffortNumber { get; set; }
    }
}

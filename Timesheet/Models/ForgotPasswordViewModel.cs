﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
    }
}

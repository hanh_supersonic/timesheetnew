﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Models
{
    public class ProjectViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Company { get; set; }

        public string Description { get; set; }

        public string QuotationNumber { get; set; }

        public string OrderNumber { get; set; }

        public string CancelDate { get; set; }

        public ApplicationUser CreatedBy { get; set; }

        public string CreatedAt { get; set; }

        public ApplicationUser UpdatedBy { get; set; }

        public string UpdatedAt { get; set; }

        public string HourUltilized { get; set; }

        public int Status { get; set; }

        public string PlannedStart { get; set; }

        public string PlannedEnd { get; set; }


    }
}

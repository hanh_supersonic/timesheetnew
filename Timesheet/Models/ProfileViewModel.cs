﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class ProfileViewModel
    {
        public string Fullname { get; set; }

        public string Phone { get; set; }

        public IFormFile Avatar { get; set; }
    }
}

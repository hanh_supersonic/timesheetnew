﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class CreateStageTwoViewModel
    {
        public string MemberId { get; set; }
        public string ProjectId { get; set; }

        public List<int> Positions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Models
{
    public class UpdatedEffortByMemberViewModel
    {
        public string Email { get; set; }

        public string Fullname { get; set; }

        public List<CurrentEffort> CurrentEfforts { get; set; }
    }
}

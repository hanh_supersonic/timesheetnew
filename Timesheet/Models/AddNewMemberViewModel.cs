﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class AddNewMemberViewModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public List<string> RoleNames { get; set; }
    }
}

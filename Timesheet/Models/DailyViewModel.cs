﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Models
{
    public class DailyViewModel
    {
        public string ProjectCode { get; set; }

        public string PlannedStart { get; set; }

        public string PlannedEnd { get; set; }

        public List<CurrentEffort> CurrentEfforts { get; set; }
        public List<Effort> Efforts { get; set; }

        public int Status { get; set; }

        public string UtilizationTimesheet { get; set; }
    }
}

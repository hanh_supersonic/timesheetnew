﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Models
{
    public class ProjectMemberViewModel
    {
        public string ProjectId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public List<Position> Positions { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class UpdateRoleOfIndividualMemberViewModel
    {
        public string UserId { get; set; }

        public List<string> RoleNames { get; set; }
    }
}

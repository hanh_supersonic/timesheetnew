﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Models
{
    public class LeaveBalanceConfigViewModel
    {
        public int Id { get; set; }

        public int TimiosStaffId { get; set; }

        public string Designation { get; set; }

        public double GrantedSL { get; set; }

        public double GrantedBF { get; set; }

        public double GrantedAL { get; set; }

        public double TotalAL { get; set; }

        public double BalanceAL { get; set; }

        public double TotalML { get; set; }

        public double BalanceML { get; set; }

        public double TotalMPL { get; set; }

        public double TakenMPL { get; set; }

        public double BalanceMPL { get; set; }

        public double TotalCL { get; set; }

        public double TakenCL { get; set; }

        public double BalanceCL { get; set; }

        public int Year { get; set; }
    }
}

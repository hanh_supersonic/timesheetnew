﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Models
{
    public class LeaveModel
    {
        public TimiosStaff StaffModel { get; set; }
        public LeaveRequest LeaveRequestModel { get; set; }
        public LeaveBalance LeaveBalanceModel { get; set; }

    }
}

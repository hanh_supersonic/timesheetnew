﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class StageOneViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Company { get; set; }

        public string Description { get; set; }

        public string QuotationNumber { get; set; }

        public string OrderNumber { get; set; }

        public string PlannedStart { get; set; }

        public string PlannedEnd { get; set; }

        public string HourUltilized { get; set; }

        public string Activity { get; set; }

        public List<string> Efforts { get; set; }

        public int Status { get; set; }

    }
}

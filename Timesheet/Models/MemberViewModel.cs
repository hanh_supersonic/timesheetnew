﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Models
{
    public class MemberViewModel
    {
        public ApplicationUser User { get; set; }

        public List<IdentityRole> Roles { get; set; }

        public List<IdentityRole> AllRoles { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class LockMemberViewModel
    {
        public string Username { get; set; }

        public string UserId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Models
{
    public class IndexModel
    {
        public TimiosStaff StaffModel { get; set; }
        public LeaveBalance LeaveBalanceModel { get; set; }
        public List<LeaveRequest> LeaveRequestModelList { get; set; }


        public string SearchYear { get; set; }
        public int? SearchLeaveTypeID { get; set; }
        public int? SearchStaffID { get; set; }
        public string SearchStatus { get; set; }
    }
}

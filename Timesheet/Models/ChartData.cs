﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class ChartData
    {
        public int count { get; set; }
        public string name { get; set; }
    }
}

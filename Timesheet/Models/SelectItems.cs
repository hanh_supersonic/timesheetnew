﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Models
{
    public class SelectItems
    {
        public List<SelectListItem> LeaveTypeListItem { get; set; }
        public List<SelectListItem> StaffListItem { get; set; }

        public SelectItems()
        {
            LeaveTypeListItem = new List<SelectListItem>();
            StaffListItem = new List<SelectListItem>();
        }
    }
}

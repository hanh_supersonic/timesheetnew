﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ManagerInChanrge",
                table: "LeaveRequests");

            migrationBuilder.AddColumn<int>(
                name: "ManagerInCharge",
                table: "LeaveRequests",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ManagerInCharge",
                table: "LeaveRequests");

            migrationBuilder.AddColumn<int>(
                name: "ManagerInChanrge",
                table: "LeaveRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

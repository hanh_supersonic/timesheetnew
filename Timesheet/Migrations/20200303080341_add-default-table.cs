﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class adddefaulttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActivityDefaults",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityDefaults", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EffortDefaults",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ActivityDefaultId = table.Column<int>(nullable: true),
                    Name = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EffortDefaults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EffortDefaults_ActivityDefaults_ActivityDefaultId",
                        column: x => x.ActivityDefaultId,
                        principalTable: "ActivityDefaults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EffortDefaults_ActivityDefaultId",
                table: "EffortDefaults",
                column: "ActivityDefaultId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EffortDefaults");

            migrationBuilder.DropTable(
                name: "ActivityDefaults");
        }
    }
}

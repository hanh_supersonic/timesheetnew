﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class updatedatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProjectId",
                table: "ProjectActivities",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectId",
                table: "Efforts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProjectActivities_ProjectId",
                table: "ProjectActivities",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Efforts_ProjectId",
                table: "Efforts",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Efforts_Projects_ProjectId",
                table: "Efforts",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectActivities_Projects_ProjectId",
                table: "ProjectActivities",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Efforts_Projects_ProjectId",
                table: "Efforts");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectActivities_Projects_ProjectId",
                table: "ProjectActivities");

            migrationBuilder.DropIndex(
                name: "IX_ProjectActivities_ProjectId",
                table: "ProjectActivities");

            migrationBuilder.DropIndex(
                name: "IX_Efforts_ProjectId",
                table: "Efforts");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "ProjectActivities");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "Efforts");
        }
    }
}

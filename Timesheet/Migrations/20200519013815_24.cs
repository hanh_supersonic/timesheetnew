﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _24 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BalanceMPCL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "TakenMPCL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "TotalMPCL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "BalanceMPCL",
                table: "LeaveBalanceConfigs");

            migrationBuilder.DropColumn(
                name: "TotalMPCL",
                table: "LeaveBalanceConfigs");

            //migrationBuilder.AlterColumn<string>(
            //    name: "Id",
            //    table: "TimiosStaffs",
            //    nullable: false,
            //    oldClrType: typeof(int),
            //    oldType: "int")
            //    .OldAnnotation("SqlServer:Identity", "1, 1");

            //migrationBuilder.AlterColumn<string>(
            //    name: "TimiosStaffId",
            //    table: "LeaveRequests",
            //    nullable: true,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            //migrationBuilder.AlterColumn<string>(
            //    name: "SubmitStaffId",
            //    table: "LeaveRequests",
            //    nullable: true,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            //migrationBuilder.AlterColumn<string>(
            //    name: "LastUpdatedById",
            //    table: "LeaveRequests",
            //    nullable: true,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            //migrationBuilder.AlterColumn<string>(
            //    name: "StaffId",
            //    table: "LeaveBalances",
            //    nullable: true,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BalanceCL",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "BalanceMPL",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TakenCL",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TakenMPL",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalCL",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalMPL",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AlterColumn<string>(
                name: "TimiosStaffId",
                table: "LeaveBalanceConfigs",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BalanceCL",
                table: "LeaveBalanceConfigs",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "BalanceMPL",
                table: "LeaveBalanceConfigs",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalCL",
                table: "LeaveBalanceConfigs",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalMPL",
                table: "LeaveBalanceConfigs",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BalanceCL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "BalanceMPL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "TakenCL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "TakenMPL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "TotalCL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "TotalMPL",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "BalanceCL",
                table: "LeaveBalanceConfigs");

            migrationBuilder.DropColumn(
                name: "BalanceMPL",
                table: "LeaveBalanceConfigs");

            migrationBuilder.DropColumn(
                name: "TotalCL",
                table: "LeaveBalanceConfigs");

            migrationBuilder.DropColumn(
                name: "TotalMPL",
                table: "LeaveBalanceConfigs");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "TimiosStaffs",
                type: "int",
                nullable: false,
                oldClrType: typeof(string))
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "TimiosStaffId",
                table: "LeaveRequests",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SubmitStaffId",
                table: "LeaveRequests",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LastUpdatedById",
                table: "LeaveRequests",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "StaffId",
                table: "LeaveBalances",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BalanceMPCL",
                table: "LeaveBalances",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TakenMPCL",
                table: "LeaveBalances",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalMPCL",
                table: "LeaveBalances",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AlterColumn<int>(
                name: "TimiosStaffId",
                table: "LeaveBalanceConfigs",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<double>(
                name: "BalanceMPCL",
                table: "LeaveBalanceConfigs",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalMPCL",
                table: "LeaveBalanceConfigs",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}

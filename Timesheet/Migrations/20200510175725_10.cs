﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Contact",
                table: "TimiosStaffs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "TimiosStaffs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Contact",
                table: "TimiosStaffs");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "TimiosStaffs");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LeaveBalanceConfigs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TimiosStaffId = table.Column<string>(nullable: true),
                    GrantedAL = table.Column<double>(nullable: false),
                    TotalAL = table.Column<double>(nullable: false),
                    BalanceAL = table.Column<double>(nullable: false),
                    TotalML = table.Column<double>(nullable: false),
                    BalanceML = table.Column<double>(nullable: false),
                    TotalMPCL = table.Column<double>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveBalanceConfigs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeaveBalanceConfigs_TimiosStaffs_TimiosStaffId",
                        column: x => x.TimiosStaffId,
                        principalTable: "TimiosStaffs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LeaveBalanceConfigs_TimiosStaffId",
                table: "LeaveBalanceConfigs",
                column: "TimiosStaffId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LeaveBalanceConfigs");
        }
    }
}

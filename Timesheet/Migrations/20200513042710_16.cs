﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _16 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "GrantedBF",
                table: "LeaveBalanceConfigs",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "GrantedSL",
                table: "LeaveBalanceConfigs",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GrantedBF",
                table: "LeaveBalanceConfigs");

            migrationBuilder.DropColumn(
                name: "GrantedSL",
                table: "LeaveBalanceConfigs");
        }
    }
}

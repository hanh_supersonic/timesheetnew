﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AssignedMemberId",
                table: "Efforts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AssignedMemberId",
                table: "CurrentEfforts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Efforts_AssignedMemberId",
                table: "Efforts",
                column: "AssignedMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_CurrentEfforts_AssignedMemberId",
                table: "CurrentEfforts",
                column: "AssignedMemberId");

            migrationBuilder.AddForeignKey(
                name: "FK_CurrentEfforts_AspNetUsers_AssignedMemberId",
                table: "CurrentEfforts",
                column: "AssignedMemberId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Efforts_AspNetUsers_AssignedMemberId",
                table: "Efforts",
                column: "AssignedMemberId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CurrentEfforts_AspNetUsers_AssignedMemberId",
                table: "CurrentEfforts");

            migrationBuilder.DropForeignKey(
                name: "FK_Efforts_AspNetUsers_AssignedMemberId",
                table: "Efforts");

            migrationBuilder.DropIndex(
                name: "IX_Efforts_AssignedMemberId",
                table: "Efforts");

            migrationBuilder.DropIndex(
                name: "IX_CurrentEfforts_AssignedMemberId",
                table: "CurrentEfforts");

            migrationBuilder.DropColumn(
                name: "AssignedMemberId",
                table: "Efforts");

            migrationBuilder.DropColumn(
                name: "AssignedMemberId",
                table: "CurrentEfforts");
        }
    }
}

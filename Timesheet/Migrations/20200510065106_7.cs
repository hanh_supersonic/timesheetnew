﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaveRequests_AspNetUsers_ApplicationUserId",
                table: "LeaveRequests");

            migrationBuilder.DropIndex(
                name: "IX_LeaveRequests_ApplicationUserId",
                table: "LeaveRequests");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "LeaveRequests");

            migrationBuilder.AddColumn<string>(
                name: "Fullname",
                table: "TimiosStaffs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimiosStaffId",
                table: "LeaveRequests",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AttendanceReports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PersonnelId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DeviceName = table.Column<string>(nullable: true),
                    VerifyType = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    HistStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceReports", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequests_TimiosStaffId",
                table: "LeaveRequests",
                column: "TimiosStaffId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveRequests_TimiosStaffs_TimiosStaffId",
                table: "LeaveRequests",
                column: "TimiosStaffId",
                principalTable: "TimiosStaffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaveRequests_TimiosStaffs_TimiosStaffId",
                table: "LeaveRequests");

            migrationBuilder.DropTable(
                name: "AttendanceReports");

            migrationBuilder.DropIndex(
                name: "IX_LeaveRequests_TimiosStaffId",
                table: "LeaveRequests");

            migrationBuilder.DropColumn(
                name: "Fullname",
                table: "TimiosStaffs");

            migrationBuilder.DropColumn(
                name: "TimiosStaffId",
                table: "LeaveRequests");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "LeaveRequests",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequests_ApplicationUserId",
                table: "LeaveRequests",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveRequests_AspNetUsers_ApplicationUserId",
                table: "LeaveRequests",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

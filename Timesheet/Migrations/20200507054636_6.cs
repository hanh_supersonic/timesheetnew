﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BizValues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastUpdatedById = table.Column<string>(nullable: true),
                    LastUpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BizValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BizValues_AspNetUsers_LastUpdatedById",
                        column: x => x.LastUpdatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LeaveBalances",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Year = table.Column<DateTime>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: true),
                    GrantedBF = table.Column<double>(nullable: false),
                    GrantedAL = table.Column<double>(nullable: false),
                    GrantedSL = table.Column<double>(nullable: false),
                    TotalAL = table.Column<double>(nullable: false),
                    TakenAL = table.Column<double>(nullable: false),
                    BalanceAL = table.Column<double>(nullable: false),
                    TotalMPCL = table.Column<double>(nullable: false),
                    TakenMPCL = table.Column<double>(nullable: false),
                    BalanceMPCL = table.Column<double>(nullable: false),
                    LastUpdatedById = table.Column<string>(nullable: true),
                    LastUpdatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveBalances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeaveBalances_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LeaveBalances_AspNetUsers_LastUpdatedById",
                        column: x => x.LastUpdatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LeaveRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApplicationUserId = table.Column<string>(nullable: true),
                    ManagerInChanrge = table.Column<int>(nullable: false),
                    LeaveTypeId = table.Column<int>(nullable: false),
                    LeaveTypeOthers = table.Column<string>(nullable: true),
                    NoOfDays = table.Column<double>(nullable: false),
                    Mon1 = table.Column<bool>(nullable: false),
                    Mon2 = table.Column<bool>(nullable: false),
                    Tue1 = table.Column<bool>(nullable: false),
                    Tue2 = table.Column<bool>(nullable: false),
                    Wed1 = table.Column<bool>(nullable: false),
                    Wed2 = table.Column<bool>(nullable: false),
                    Thu1 = table.Column<bool>(nullable: false),
                    Thu2 = table.Column<bool>(nullable: false),
                    Fri1 = table.Column<bool>(nullable: false),
                    Fri2 = table.Column<bool>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    StartAM = table.Column<bool>(nullable: false),
                    StartPM = table.Column<bool>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    EndAM = table.Column<bool>(nullable: false),
                    EndPM = table.Column<bool>(nullable: false),
                    SubmitDate = table.Column<DateTime>(nullable: false),
                    SubmitStaffIdId = table.Column<string>(nullable: true),
                    LeaveStatus = table.Column<string>(nullable: true),
                    LastUpdatedById = table.Column<string>(nullable: true),
                    LastUpdatedOn = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeaveRequests_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LeaveRequests_AspNetUsers_LastUpdatedById",
                        column: x => x.LastUpdatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LeaveRequests_AspNetUsers_SubmitStaffIdId",
                        column: x => x.SubmitStaffIdId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TimiosStaffs",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: true),
                    Designation = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Atd_Device = table.Column<int>(nullable: false),
                    DevicePass = table.Column<string>(nullable: true),
                    TeamLeaderId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimiosStaffs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TimiosStaffs_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TimiosStaffs_AspNetUsers_TeamLeaderId",
                        column: x => x.TeamLeaderId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BizValues_LastUpdatedById",
                table: "BizValues",
                column: "LastUpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_LeaveBalances_ApplicationUserId",
                table: "LeaveBalances",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LeaveBalances_LastUpdatedById",
                table: "LeaveBalances",
                column: "LastUpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequests_ApplicationUserId",
                table: "LeaveRequests",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequests_LastUpdatedById",
                table: "LeaveRequests",
                column: "LastUpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequests_SubmitStaffIdId",
                table: "LeaveRequests",
                column: "SubmitStaffIdId");

            migrationBuilder.CreateIndex(
                name: "IX_TimiosStaffs_ApplicationUserId",
                table: "TimiosStaffs",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TimiosStaffs_TeamLeaderId",
                table: "TimiosStaffs",
                column: "TeamLeaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BizValues");

            migrationBuilder.DropTable(
                name: "LeaveBalances");

            migrationBuilder.DropTable(
                name: "LeaveRequests");

            migrationBuilder.DropTable(
                name: "TimiosStaffs");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Company = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    QuotationNumber = table.Column<string>(nullable: true),
                    OrderNumber = table.Column<string>(nullable: true),
                    CancelDate = table.Column<string>(nullable: true),
                    CreatedById = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_AspNetUsers_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_AspNetUsers_UpdatedById",
                        column: x => x.UpdatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectConfigs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<string>(nullable: true),
                    HourUltilized = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<string>(nullable: true),
                    IsUsed = table.Column<bool>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    PlannedStart = table.Column<string>(nullable: true),
                    PlannedEnd = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectConfigs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectConfigs_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectActivities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectConfigId = table.Column<int>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectActivities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectActivities_ProjectConfigs_ProjectConfigId",
                        column: x => x.ProjectConfigId,
                        principalTable: "ProjectConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Efforts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectActivityId = table.Column<int>(nullable: true),
                    StageName = table.Column<string>(nullable: true),
                    EffortNumber = table.Column<string>(nullable: true),
                    CreatedById = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Efforts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Efforts_AspNetUsers_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Efforts_ProjectActivities_ProjectActivityId",
                        column: x => x.ProjectActivityId,
                        principalTable: "ProjectActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Efforts_AspNetUsers_UpdatedById",
                        column: x => x.UpdatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CurrentEfforts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<string>(nullable: true),
                    EffortId = table.Column<int>(nullable: true),
                    Date = table.Column<string>(nullable: true),
                    StageName = table.Column<string>(nullable: true),
                    UpdatedById = table.Column<string>(nullable: true),
                    EffortNumber = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrentEfforts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CurrentEfforts_Efforts_EffortId",
                        column: x => x.EffortId,
                        principalTable: "Efforts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CurrentEfforts_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CurrentEfforts_AspNetUsers_UpdatedById",
                        column: x => x.UpdatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CurrentEfforts_EffortId",
                table: "CurrentEfforts",
                column: "EffortId");

            migrationBuilder.CreateIndex(
                name: "IX_CurrentEfforts_ProjectId",
                table: "CurrentEfforts",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_CurrentEfforts_UpdatedById",
                table: "CurrentEfforts",
                column: "UpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Efforts_CreatedById",
                table: "Efforts",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Efforts_ProjectActivityId",
                table: "Efforts",
                column: "ProjectActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Efforts_UpdatedById",
                table: "Efforts",
                column: "UpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectActivities_ProjectConfigId",
                table: "ProjectActivities",
                column: "ProjectConfigId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectConfigs_ProjectId",
                table: "ProjectConfigs",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CreatedById",
                table: "Projects",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_UpdatedById",
                table: "Projects",
                column: "UpdatedById");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrentEfforts");

            migrationBuilder.DropTable(
                name: "Efforts");

            migrationBuilder.DropTable(
                name: "ProjectActivities");

            migrationBuilder.DropTable(
                name: "ProjectConfigs");

            migrationBuilder.DropTable(
                name: "Projects");
        }
    }
}

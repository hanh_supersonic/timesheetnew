﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaveRequests_AspNetUsers_SubmitStaffIdId",
                table: "LeaveRequests");

            migrationBuilder.DropIndex(
                name: "IX_LeaveRequests_SubmitStaffIdId",
                table: "LeaveRequests");

            migrationBuilder.DropColumn(
                name: "SubmitStaffIdId",
                table: "LeaveRequests");

            migrationBuilder.AddColumn<string>(
                name: "SubmitStaffId",
                table: "LeaveRequests",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequests_SubmitStaffId",
                table: "LeaveRequests",
                column: "SubmitStaffId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveRequests_TimiosStaffs_SubmitStaffId",
                table: "LeaveRequests",
                column: "SubmitStaffId",
                principalTable: "TimiosStaffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaveRequests_TimiosStaffs_SubmitStaffId",
                table: "LeaveRequests");

            migrationBuilder.DropIndex(
                name: "IX_LeaveRequests_SubmitStaffId",
                table: "LeaveRequests");

            migrationBuilder.DropColumn(
                name: "SubmitStaffId",
                table: "LeaveRequests");

            migrationBuilder.AddColumn<string>(
                name: "SubmitStaffIdId",
                table: "LeaveRequests",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequests_SubmitStaffIdId",
                table: "LeaveRequests",
                column: "SubmitStaffIdId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveRequests_AspNetUsers_SubmitStaffIdId",
                table: "LeaveRequests",
                column: "SubmitStaffIdId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

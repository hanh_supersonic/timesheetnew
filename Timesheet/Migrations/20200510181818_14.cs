﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _14 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaveBalances_TimiosStaffs_LastUpdatedById",
                table: "LeaveBalances");

            migrationBuilder.DropIndex(
                name: "IX_LeaveBalances_LastUpdatedById",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "LastUpdatedById",
                table: "LeaveBalances");

            migrationBuilder.AddColumn<string>(
                name: "LastUpdatedBy",
                table: "LeaveBalances",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastUpdatedBy",
                table: "LeaveBalances");

            migrationBuilder.AddColumn<string>(
                name: "LastUpdatedById",
                table: "LeaveBalances",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeaveBalances_LastUpdatedById",
                table: "LeaveBalances",
                column: "LastUpdatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveBalances_TimiosStaffs_LastUpdatedById",
                table: "LeaveBalances",
                column: "LastUpdatedById",
                principalTable: "TimiosStaffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

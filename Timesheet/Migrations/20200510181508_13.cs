﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Timesheet.Migrations
{
    public partial class _13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaveBalances_AspNetUsers_ApplicationUserId",
                table: "LeaveBalances");

            migrationBuilder.DropForeignKey(
                name: "FK_LeaveBalances_AspNetUsers_LastUpdatedById",
                table: "LeaveBalances");

            migrationBuilder.DropIndex(
                name: "IX_LeaveBalances_ApplicationUserId",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "LeaveBalances");

            migrationBuilder.AddColumn<double>(
                name: "BalanceML",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "StaffId",
                table: "LeaveBalances",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "TakenML",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalML",
                table: "LeaveBalances",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateIndex(
                name: "IX_LeaveBalances_StaffId",
                table: "LeaveBalances",
                column: "StaffId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveBalances_TimiosStaffs_LastUpdatedById",
                table: "LeaveBalances",
                column: "LastUpdatedById",
                principalTable: "TimiosStaffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveBalances_TimiosStaffs_StaffId",
                table: "LeaveBalances",
                column: "StaffId",
                principalTable: "TimiosStaffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeaveBalances_TimiosStaffs_LastUpdatedById",
                table: "LeaveBalances");

            migrationBuilder.DropForeignKey(
                name: "FK_LeaveBalances_TimiosStaffs_StaffId",
                table: "LeaveBalances");

            migrationBuilder.DropIndex(
                name: "IX_LeaveBalances_StaffId",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "BalanceML",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "StaffId",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "TakenML",
                table: "LeaveBalances");

            migrationBuilder.DropColumn(
                name: "TotalML",
                table: "LeaveBalances");

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "LeaveBalances",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LeaveBalances_ApplicationUserId",
                table: "LeaveBalances",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveBalances_AspNetUsers_ApplicationUserId",
                table: "LeaveBalances",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LeaveBalances_AspNetUsers_LastUpdatedById",
                table: "LeaveBalances",
                column: "LastUpdatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

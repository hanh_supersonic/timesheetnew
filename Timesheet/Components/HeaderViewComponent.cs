﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Models;
using Timesheet.Repository;

namespace Timesheet.Components
{
    public class HeaderViewComponent : ViewComponent
    {
        readonly UserManager<ApplicationUser> _userManager;
        readonly IUserRepository _userRepository;
        [Obsolete]
        readonly IHostingEnvironment _hostingEnvironment;

        [Obsolete]
        public HeaderViewComponent(UserManager<ApplicationUser> userManager, IUserRepository userRepository,
                                IHostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _userRepository = userRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var currentUser = this.HttpContext.User;
            var user = await _userManager.GetUserAsync(currentUser);
            if (user.FullName == null)
            {
                user.FullName = "Blank";
            }

            if (user.Avatar == null || user.Avatar == "")
            {
                user.Avatar = "avatar-1.png";
            }


            return View(user);
        }
    }
}

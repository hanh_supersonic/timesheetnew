﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;
using Timesheet.Repository;

namespace Timesheet.Components
{
    public class SidebarViewComponent : ViewComponent
    {
        Dictionary<string, List<Header>> headersDic = new Dictionary<string, List<Header>>();

        readonly UserManager<ApplicationUser> _userManager;
        readonly IUserRepository _userRepository;
        [Obsolete]
        readonly IHostingEnvironment _hostingEnvironment;

        [Obsolete]
        public SidebarViewComponent(UserManager<ApplicationUser> userManager, IUserRepository userRepository,
                                IHostingEnvironment hostingEnvironment)
        {
            _userManager = userManager;
            _userRepository = userRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            headersDic.Add("DASHBOARD", new List<Header>()
            {
                new Header()
                {
                    Id = 1, Icon = "fas fa-fire", Name = "Dashboard", Order = 1, ParentId = 0, DoesHaveChild = true,
                    RoleNames = new List<string>(){"admin"}, Link = "#", Active = ""
                },
                new Header()
                {
                    Id = 2, Icon = "fas fa-fire", Name = "Today", Order = 2, ParentId = 1, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin"}, Link = "/Dashboard/Today", Active = ""
                },
                new Header()
                {
                    Id = 3, Icon = "fas fa-fire", Name = "General", Order = 3, ParentId = 1, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin"}, Link = "/Dashboard/General", Active = ""
                },
            }); ;

            headersDic.Add("WORKING", new List<Header>()
            {
                new Header()
                {
                    Id = 4, Icon = "fas fa-bicycle", Name = "Daily", Order = 1, ParentId = 0, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin","member"}, Link = "/Daily", Active = ""
                },
                new Header()
                {
                    Id = 5, Icon = "fas fa-th", Name = "Projects", Order = 2, ParentId = 0, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin"}, Link = "/Project", Active = ""
                },
                new Header()
                {
                    Id = 6, Icon = "far fa-user", Name = "Member", Order = 3, ParentId = 0, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin"}, Link = "/Member", Active = ""
                },
            });

            headersDic.Add("ATTENDANCE", new List<Header>()
            {
                new Header()
                {
                    Id = 7, Icon = "fas fa-bed", Name = "Attendance", Order = 1, ParentId = 0, DoesHaveChild = true,
                    RoleNames = new List<string>(){"admin", "member"}, Link = "/Attendance", Active = ""
                },
                new Header()
                {
                    Id = 8, Icon = "fas fa-bed", Name = "Leave Type", Order = 2, ParentId = 7, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin"}, Link = "/LeaveType", Active = ""
                },
                new Header()
                {
                    Id = 9, Icon = "fas fa-bed", Name = "Attendance", Order = 2, ParentId = 7, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin"}, Link = "/Attend", Active = ""
                },
                new Header()
                {
                    Id = 10, Icon = "fas fa-bed", Name = "Calendar", Order = 2, ParentId = 7, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin", "member"}, Link = "/Calendar", Active = ""
                },
                new Header()
                {
                    Id = 11, Icon = "fas fa-bed", Name = "Leave Request", Order = 2, ParentId = 7, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin", "member"}, Link = "/LeaveRequest", Active = ""
                },
                 new Header()
                {
                    Id = 11, Icon = "fas fa-bed", Name = "Leave Balance", Order = 2, ParentId = 7, DoesHaveChild = false,
                    RoleNames = new List<string>(){"admin"}, Link = "/LeaveBalanceConfig", Active = ""
                },
            });

            var host = HttpContext.Request.Host;
            var path = HttpContext.Request.Path;
            

            var currentUser = this.HttpContext.User;
            var user = await _userManager.GetUserAsync(currentUser);
            var currentRoles = await _userManager.GetRolesAsync(user);

            foreach (var kvp in headersDic)
            {
                foreach (var header in kvp.Value)
                {
                    var currentUrl = host + path;
                    if (currentUrl.ToLower().Contains( (host + header.Link).ToLower() )) 
                    {
                        header.Active = "active";
                    }
                    else
                    {
                        header.Active = "";
                    }
                    header.IsShown = header.RoleNames.Any(x => currentRoles.Contains(x) == true);
                }
            }

            return View(headersDic);
        }
    }
}

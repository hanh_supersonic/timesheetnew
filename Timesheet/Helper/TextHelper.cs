﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Helper
{
    public static class TextHelper
    {
        public static string UpperFirstLetter(this string text)
        {
            return text.First().ToString().ToUpper() + text.Substring(1);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Models;
using Timesheet.Repository;

using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;
using MimeKit;

namespace Timesheet.Controllers
{
    [Authorize(Roles = "admin")]
    //[AllowAnonymous]
    public class MemberController : Controller
    {
        RoleManager<IdentityRole> _roleManager;
        UserManager<ApplicationUser> _userManager;
        readonly IUserRepository _userRepository;
        readonly IRoleRepository _roleRepository;
        readonly IActivityRepository _activityRepository;
        readonly ICurrentEffortRepository _currentEffortRepository;
        readonly ITimiosStaffRepository _timiosStaffRepository;
        readonly ILeaveBalanceRepository _leaveBalanceRepository;
        readonly ILeaveBalanceConfigRepository _leaveBalanceConfigRepository;

        public MemberController(RoleManager<IdentityRole> roleManager, IUserRepository userRepository,
                                IRoleRepository roleRepository, UserManager<ApplicationUser> userManager,
                                IActivityRepository activityRepository, ICurrentEffortRepository currentEffortRepository,
                                ITimiosStaffRepository timiosStaffRepository, ILeaveBalanceRepository leaveBalanceRepository,
                                ILeaveBalanceConfigRepository leaveBalanceConfigRepository)
        {
            _roleManager = roleManager;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _userManager = userManager;
            _activityRepository = activityRepository;
            _currentEffortRepository = currentEffortRepository;
            _timiosStaffRepository = timiosStaffRepository;
            _leaveBalanceRepository = leaveBalanceRepository;
            _leaveBalanceConfigRepository = leaveBalanceConfigRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<MemberViewModel> memberViewModels = new List<MemberViewModel>();

            var users = _userRepository.GetAll().OrderByDescending(x => x.Id).ToList();

            foreach (var user in users)
            {
                var roles = _roleRepository.GetByUser(user.Id).ToList(); ;
                memberViewModels.Add(new MemberViewModel()
                {
                    User = user,
                    Roles = roles,
                });
            }
            ViewBag.AllRoles = _roleRepository.GetAll();

            return View(memberViewModels);
        }

        [HttpGet]
        public MemberViewModel GetInforOfIndividualMember(string userId)
        {
            var user = _userRepository.GetSingleById(userId);

            var roles = _roleRepository.GetByUser(user.Id).ToList(); ;
            MemberViewModel memberViewModel = new MemberViewModel()
            {
                User = user,
                Roles = roles,
                AllRoles = _roleRepository.GetAll().ToList()
            };
            return memberViewModel;
        }

        [HttpGet]
        public UpdatedEffortByMemberViewModel GetEffortByMemberViewModel(string userId)
        {
            var user = _userRepository.GetSingleById(userId);
            var currentEfforts = _currentEffortRepository.GetAllByUpdatedBy(userId).ToList();
            UpdatedEffortByMemberViewModel updatedEffortByMemberViewModel = new UpdatedEffortByMemberViewModel()
            {
                Email = user.Email,
                Fullname = user.FullName,
                CurrentEfforts = currentEfforts
            };

            return updatedEffortByMemberViewModel;
        }


        [HttpPost] 
        public async Task<IActionResult> UpdateRoleOfIndividualMember(UpdateRoleOfIndividualMemberViewModel updateRoleOfIndividualMemberViewModel)
        {
            var user = _userRepository.GetSingleById(updateRoleOfIndividualMemberViewModel.UserId);
            if (user.Email == "hanh@supersonic.sg")
            {
                return RedirectToAction("Index", "Member");
            }
            var currentRoles = await _userManager.GetRolesAsync(user);
            foreach (var currentRole in currentRoles)
            {
                await _userManager.RemoveFromRoleAsync(user, currentRole);

            }

            if (updateRoleOfIndividualMemberViewModel.RoleNames != null)
            {
                await _userManager.AddToRolesAsync(user, updateRoleOfIndividualMemberViewModel.RoleNames);
            }

            var currentUser = this.User;
            var currentUserId = _userManager.GetUserId(currentUser);

            Activity activity = new Activity()
            {
                ApplicationUser = _userRepository.GetSingleById(currentUserId),
                Icon = "far fa-user",
                CreatedAt = DateTime.Now,
                Content = "Update roles of member",
            };
            _activityRepository.Add(activity);

            return RedirectToAction("Index", "Member");
        }

        [HttpPost]
        public async Task<IActionResult> LockMember(LockMemberViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userRepository.GetSingleById(model.UserId);
                _userRepository.DeleteMember(user);

                TimiosStaff timiosStaff = _timiosStaffRepository.GetStaffByEmail(user.Email);
                if (timiosStaff != null)
                {
                    _timiosStaffRepository.LockByUserId(user.Id);
                    _leaveBalanceConfigRepository.LockByTimiosStaff(timiosStaff.Id.ToString());
                }

                var currentUser = this.User;
                var currentUserId = _userManager.GetUserId(currentUser);
                Activity activity = new Activity()
                {
                    ApplicationUser = _userRepository.GetSingleById(currentUserId),
                    Icon = "far fa-user",
                    CreatedAt = DateTime.Now,
                    Content = "Delete member",
                };
                _activityRepository.Add(activity);

                await _userManager.UpdateSecurityStampAsync(user);
            }
            return RedirectToAction("Index", "Member");
        }

        [HttpPost]
        public async Task<IActionResult> AddMemberToSystem(AddNewMemberViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, FullName = model.Name };
                string password = GetRandomPassword();
                //string password = "SingViet2020";
                var result = await _userManager.CreateAsync(user, password);


                if (result.Succeeded)
                {
                    if (model.RoleNames != null || model.RoleNames.Count != 0)
                    {
                        await _userManager.AddToRolesAsync(user, model.RoleNames);
                    }

                    var currentUser = this.User;
                    var currentUserId = _userManager.GetUserId(currentUser);
                    var index = _timiosStaffRepository.GetAll().Count() + 1;

                    TimiosStaff timiosStaff = new TimiosStaff()
                    {
                        Id = index.ToString(),
                        Active = true,
                        ApplicationUser = user,
                        Atd_Device = -1,
                        Contact = "",
                        Designation = "Blank",
                        Email = user.Email,
                        Fullname = user.FullName,
                    };

                    _timiosStaffRepository.Add(timiosStaff);

                    LeaveBalanceConfig leaveBalanceConfig = new LeaveBalanceConfig()
                    {
                        TimiosStaff = timiosStaff,
                        TotalAL = 10,
                        TotalML = 20,
                        Active = true,
                        BalanceAL = 10,
                        BalanceML = 20,
                        GrantedAL = 10,
                        GrantedSL = 0,
                        GrantedBF = 0,
                        BalanceMPL = 10,
                        TotalMPL = 10,
                        BalanceCL = 10,
                        TotalCL = 10,
                        Year = DateTime.Now.Year
                    };
                    _leaveBalanceConfigRepository.Add(leaveBalanceConfig);

                    // send mail
                    create_email(user.Email, "Account", @"Your account here: <br> Username: " + model.Email + "<br> Password: " + password);


                    Activity activity = new Activity()
                    {
                        ApplicationUser = _userRepository.GetSingleById(currentUserId),
                        Icon = "far fa-user",
                        CreatedAt = DateTime.Now,
                        Content = "Add new member to system",
                    };
                    _activityRepository.Add(activity);
                }
            }
            return RedirectToAction("Index", "Member");
        }

        public void create_email(string email, string subject, string body)
        {
            Microsoft.AspNet.Identity.IdentityMessage IM = new Microsoft.AspNet.Identity.IdentityMessage();
            IM.Destination = email;
            IM.Subject = subject;
            IM.Body = body;
            configSendMailKit(IM);
        }

        public static void configSendMailKit(Microsoft.AspNet.Identity.IdentityMessage message)
        {
            var myMessage = new MimeMessage();
            myMessage.From.Add(new MailboxAddress("Admin", "Admin@timios.net"));
            myMessage.To.Add(new MailboxAddress("User", message.Destination));
            myMessage.Sender = (new MailboxAddress("Admin", "Admin@timios.net"));
            myMessage.Subject = message.Subject;
            myMessage.Body = new TextPart("plain") { Text = message.Body };

            try
            {
                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("in-v3.mailjet.com", 587, SecureSocketOptions.Auto);

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate("8d179afcd3cdb4c0d8d6314d9bc47fcd", "628a7dcd4f879e58330baa7368475dc3");
                    client.Send(myMessage);
                    var smtp = new SmtpClient(new ProtocolLogger(Console.OpenStandardOutput()));

                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }
        }

        private string GetRandomPassword()
        {
            var letters = "0123456789ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghjklmnopqrstuvwxyz!@#$%";
            var password = "";
            for (int i = 0; i < 8; i++)
            {
                password += letters.ElementAt((new Random()).Next(0, letters.Count()));
            }
            return password;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Models;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize(Roles = "admin")]
    public class LeaveBalanceConfigController : Controller
    {
        readonly ILeaveBalanceRepository _leaveBalanceRepository;
        readonly ITimiosStaffRepository _timiosStaffRepository;
        readonly ILeaveBalanceConfigRepository _leaveBalanceConfigRepository;

        public LeaveBalanceConfigController(ILeaveBalanceRepository leaveBalanceRepository, ITimiosStaffRepository timiosStaffRepository,
                                            ILeaveBalanceConfigRepository leaveBalanceConfigRepository)
        {
            _leaveBalanceRepository = leaveBalanceRepository;
            _timiosStaffRepository = timiosStaffRepository;
            _leaveBalanceConfigRepository = leaveBalanceConfigRepository;
        }

        public IActionResult Index()
        {
            List<LeaveBalanceConfig> leaveBalanceConfigs = _leaveBalanceConfigRepository.GetAll().OrderByDescending(x => x.Id).ToList();
            ViewBag.TimiosStaffs = _timiosStaffRepository.GetAllActive().ToList();

            return View(leaveBalanceConfigs);
        }

        [HttpGet]
        public LeaveBalanceConfig GetSingleById(int id)
        {
            LeaveBalanceConfig leaveBalanceConfig = _leaveBalanceConfigRepository.GetSingleById(id);
            return leaveBalanceConfig;
        }

        [HttpPost]
        public IActionResult Update(LeaveBalanceConfigViewModel leaveBalanceConfigViewModel)
        {
            LeaveBalanceConfig leaveBalanceConfig = new LeaveBalanceConfig();
            leaveBalanceConfig.GrantedAL = leaveBalanceConfigViewModel.GrantedAL;
            leaveBalanceConfig.GrantedBF = leaveBalanceConfigViewModel.GrantedBF;
            leaveBalanceConfig.GrantedSL = leaveBalanceConfigViewModel.GrantedSL;
            leaveBalanceConfig.BalanceAL = leaveBalanceConfigViewModel.BalanceAL;
            leaveBalanceConfig.TotalAL = leaveBalanceConfigViewModel.TotalAL;
            leaveBalanceConfig.BalanceML = leaveBalanceConfigViewModel.BalanceML;
            leaveBalanceConfig.TotalML = leaveBalanceConfigViewModel.TotalML;
            leaveBalanceConfig.TotalMPL = leaveBalanceConfigViewModel.TotalMPL;
            leaveBalanceConfig.BalanceMPL = leaveBalanceConfigViewModel.BalanceMPL;
            leaveBalanceConfig.TotalCL = leaveBalanceConfigViewModel.TotalCL;
            leaveBalanceConfig.BalanceCL = leaveBalanceConfigViewModel.BalanceCL;
            leaveBalanceConfig.Id = leaveBalanceConfigViewModel.Id;
            _leaveBalanceConfigRepository.Update(leaveBalanceConfig);

            TimiosStaff timiosStaff = _leaveBalanceConfigRepository.GetSingleById(leaveBalanceConfig.Id).TimiosStaff;

            LeaveBalance leaveBalance = _leaveBalanceRepository.GetLeaveBalanceByStaffIDYear(timiosStaff, leaveBalanceConfigViewModel.Year);
            leaveBalance.GrantedAL = leaveBalanceConfigViewModel.GrantedAL;
            leaveBalance.GrantedBF = leaveBalanceConfigViewModel.GrantedBF;
            leaveBalance.GrantedSL = leaveBalanceConfigViewModel.GrantedSL;
            leaveBalance.BalanceAL = leaveBalanceConfigViewModel.BalanceAL;
            leaveBalance.TotalAL = leaveBalanceConfigViewModel.TotalAL;
            leaveBalance.BalanceML = leaveBalanceConfigViewModel.BalanceML;
            leaveBalance.TotalML = leaveBalanceConfigViewModel.TotalML;
            leaveBalance.TotalMPL = leaveBalanceConfigViewModel.TotalMPL;
            leaveBalance.TakenMPL = leaveBalanceConfigViewModel.TakenMPL;
            leaveBalance.TotalMPL = leaveBalanceConfigViewModel.TotalMPL;
            leaveBalance.BalanceMPL = leaveBalanceConfigViewModel.BalanceMPL;
            leaveBalance.TakenCL = leaveBalanceConfigViewModel.TakenCL;
            leaveBalance.TotalCL = leaveBalanceConfigViewModel.TotalCL;
            leaveBalance.BalanceCL = leaveBalanceConfigViewModel.BalanceCL;
            _leaveBalanceRepository.UpdateDefaulValue(leaveBalance);

            _timiosStaffRepository.UpdateDesignation(timiosStaff.Id.ToString(), leaveBalanceConfigViewModel.Designation);
            return RedirectToAction("Index", "LeaveBalanceConfig");
        }

        public IActionResult Add(LeaveBalanceConfigViewModel leaveBalanceConfigViewModel)
        {
            List<LeaveBalanceConfig> existLeaveBalanceConfigs = _leaveBalanceConfigRepository.GetByYear(leaveBalanceConfigViewModel.Year).ToList();
            if (existLeaveBalanceConfigs == null || existLeaveBalanceConfigs.Count() == 0)
            {
                TimiosStaff timiosStaff = _timiosStaffRepository.getstaff_by_id(leaveBalanceConfigViewModel.TimiosStaffId);

                LeaveBalanceConfig leaveBalanceConfig = new LeaveBalanceConfig();
                leaveBalanceConfig.GrantedAL = leaveBalanceConfigViewModel.GrantedAL;
                leaveBalanceConfig.GrantedBF = leaveBalanceConfigViewModel.GrantedBF;
                leaveBalanceConfig.GrantedSL = leaveBalanceConfigViewModel.GrantedSL;
                leaveBalanceConfig.BalanceAL = leaveBalanceConfigViewModel.BalanceAL;
                leaveBalanceConfig.TotalAL = leaveBalanceConfigViewModel.TotalAL;
                leaveBalanceConfig.BalanceML = leaveBalanceConfigViewModel.BalanceML;
                leaveBalanceConfig.TotalML = leaveBalanceConfigViewModel.TotalML;
                leaveBalanceConfig.TotalMPL = leaveBalanceConfigViewModel.TotalMPL;
                leaveBalanceConfig.BalanceMPL = leaveBalanceConfigViewModel.BalanceMPL;
                leaveBalanceConfig.BalanceCL = leaveBalanceConfigViewModel.BalanceCL;
                leaveBalanceConfig.TotalCL = leaveBalanceConfigViewModel.TotalCL;
                leaveBalanceConfig.TimiosStaff = timiosStaff;
                leaveBalanceConfig.Year = leaveBalanceConfigViewModel.Year;
                leaveBalanceConfig.Active = true;

                _leaveBalanceConfigRepository.Add(leaveBalanceConfig);
            }

            return RedirectToAction("Index", "LeaveBalanceConfig");
        }
    }
}
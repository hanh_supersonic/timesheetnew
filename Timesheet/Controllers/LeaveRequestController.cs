﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Timesheet.Data;
using Timesheet.Models;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize]
    public class LeaveRequestController : Controller
    {
        readonly ILeaveRequestRepository _leaveRequestRepository;
        readonly ILeaveBalanceRepository _leaveBalanceRepository;
        readonly ITimiosStaffRepository _timiosStaffRepository;
        readonly IBizValueRepository _bizValueRepository;
        readonly ILeaveBalanceConfigRepository _leaveBalanceConfigRepository;

        public LeaveRequestController(ILeaveRequestRepository leaveRequestRepository, ILeaveBalanceRepository leaveBalanceRepository,
                                    ITimiosStaffRepository timiosStaffRepository, IBizValueRepository bizValueRepository,
                                    ILeaveBalanceConfigRepository leaveBalanceConfigRepository)
        {
            _leaveRequestRepository = leaveRequestRepository;
            _leaveBalanceRepository = leaveBalanceRepository;
            _timiosStaffRepository = timiosStaffRepository;
            _bizValueRepository = bizValueRepository;
            _leaveBalanceConfigRepository = leaveBalanceConfigRepository;
        }

        public LeaveBalance create_default_leaveBalance()
        {
            TimiosStaff timiosStaff = GetCurrentUser();
            LeaveBalanceConfig leaveBalanceConfig = _leaveBalanceConfigRepository.GetByTimiosStaffYear(timiosStaff, DateTime.Now.Year);
            if (leaveBalanceConfig == null)
            {
                leaveBalanceConfig = new LeaveBalanceConfig();
                leaveBalanceConfig.GrantedAL = 10;
                leaveBalanceConfig.GrantedBF = 0;
                leaveBalanceConfig.GrantedSL = 0;
                leaveBalanceConfig.BalanceAL = 10;
                leaveBalanceConfig.TotalAL = 10;
                leaveBalanceConfig.BalanceML = 20;
                leaveBalanceConfig.TotalML = 20;
                leaveBalanceConfig.TotalMPL = 10;
                leaveBalanceConfig.BalanceMPL = 10;
                leaveBalanceConfig.TotalCL = 10;
                leaveBalanceConfig.BalanceCL = 10;
                leaveBalanceConfig.TimiosStaff = timiosStaff;
                leaveBalanceConfig.Year = DateTime.Now.Year;
                leaveBalanceConfig.Active = true;
                leaveBalanceConfig.TimiosStaff = timiosStaff;
                _leaveBalanceConfigRepository.Add(leaveBalanceConfig);
            }
            LeaveBalance lb = new LeaveBalance();
            lb.GrantedBF = leaveBalanceConfig.GrantedBF;
            lb.GrantedAL = leaveBalanceConfig.GrantedAL;
            lb.GrantedSL = leaveBalanceConfig.GrantedSL;
            lb.TotalAL = leaveBalanceConfig.TotalAL;
            lb.TakenAL = 0;
            lb.BalanceAL = leaveBalanceConfig.BalanceAL;
            lb.TotalML = leaveBalanceConfig.TotalML;
            lb.TakenML = 0;
            lb.BalanceML = leaveBalanceConfig.BalanceML;
            lb.TotalMPL = leaveBalanceConfig.TotalMPL;
            lb.TakenMPL = 0;
            lb.BalanceMPL = leaveBalanceConfig.BalanceMPL;
            lb.TotalCL = leaveBalanceConfig.TotalCL;
            lb.TakenCL = 0;
            lb.BalanceCL = leaveBalanceConfig.BalanceCL;
            lb.IsDeleted = false;
            return lb;
        }

        public IActionResult Index()
        {
            IndexModel indexModel = new IndexModel();
            DateTime currentYear = DateTime.ParseExact(DateTime.Now.Year.ToString(), "yyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (User.IsInRole("admin"))
            {
                List<LeaveRequest> leaveRequestList = _leaveRequestRepository.GetLeaveRequestListBy(null, null, null, null);
                LeaveBalance leaveBalance = _leaveBalanceRepository.GetLeaveBalanceByStaffIDYear(GetCurrentUser(), currentYear, create_default_leaveBalance());

                //To force admin to have its own table uncomment this
                //indexModel.AdminLeaveRequest = _repository.GetLeaveRequestListBy(currentYear, null, GetCurrentUser().StaffID, null);

                indexModel.LeaveBalanceModel = leaveBalance;
                indexModel.LeaveRequestModelList = leaveRequestList.OrderByDescending(x => x.SubmitDate).ToList(); ;
            }
            else
            {
                if (GetCurrentUser() == null) { return RedirectToAction("No_Staff"); }

                LeaveBalance leaveBalance = _leaveBalanceRepository.GetLeaveBalanceByStaffIDYear(GetCurrentUser(), currentYear, create_default_leaveBalance());

                List<LeaveRequest> leaveRequestList = _leaveRequestRepository.GetLeaveRequestListBy(currentYear, null, int.Parse(GetCurrentUser().Id), null);

                indexModel.LeaveBalanceModel = leaveBalance;
                indexModel.LeaveRequestModelList = leaveRequestList.OrderByDescending(x => x.SubmitDate).ToList();
            }

            indexModel.StaffModel = GetCurrentUser();

            GetYears();
            GetDDL();

            var leaveBalanceConfig = _leaveBalanceConfigRepository.GetByTimiosStaffYear(GetCurrentUser(), DateTime.Now.Year);

            ViewBag.LeaveBalanceConfig = leaveBalanceConfig;

            return View(indexModel);
        }

        [HttpPost]
        public ActionResult Index(IndexModel indexModel)
        {
            GetYears();
            GetDDL();
            DateTime currentYear = DateTime.ParseExact(DateTime.Now.Year.ToString(), "yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime year = DateTime.ParseExact(indexModel.SearchYear, "yyyy", System.Globalization.CultureInfo.InvariantCulture);
            List<LeaveRequest> leaveRequestList = new List<LeaveRequest>();

            if (User.IsInRole("admin"))
            {
                // Get StaffID from DDL

                leaveRequestList = _leaveRequestRepository.GetLeaveRequestListBy(year, indexModel.SearchLeaveTypeID, indexModel.SearchStaffID, null);

                //To force admin to have its own table uncomment this
                //indexModel.AdminLeaveRequest = _repository.GetLeaveRequestListBy(year, indexModel.SearchAdminLeaveID, GetCurrentUser().StaffID, indexModel.AdminStatus);
                indexModel.LeaveBalanceModel = _leaveBalanceRepository.GetLeaveBalanceByStaffIDYear(GetCurrentUser(), currentYear, create_default_leaveBalance());
            }
            else
            {
                // Get StaffID from current user info
                leaveRequestList = _leaveRequestRepository.GetLeaveRequestListBy(year, indexModel.SearchLeaveTypeID, int.Parse(GetCurrentUser().Id), indexModel.SearchStatus);
            }

            indexModel.LeaveRequestModelList = leaveRequestList;
            var leaveBalanceConfig = _leaveBalanceConfigRepository.GetByTimiosStaffYear(GetCurrentUser(), DateTime.Now.Year);

            ViewBag.LeaveBalanceConfig = leaveBalanceConfig;

            return View(indexModel);
        }

        [HttpGet]
        public ActionResult LeaveRequest(string start, string end)
        {
            LeaveModel leaveModel = new LeaveModel();

            leaveModel.StaffModel = GetCurrentUser();


            if (start != null)
            {
                leaveModel.LeaveRequestModel = new LeaveRequest();
                DateTime startdate = Convert.ToDateTime(start);
                DateTime enddate = Convert.ToDateTime(end);
                leaveModel.LeaveRequestModel.StartDate = startdate;
                leaveModel.LeaveRequestModel.EndDate = enddate.AddDays(-1.0);

                leaveModel.LeaveRequestModel.NoOfDays = enddate.Subtract(startdate).TotalDays;

            }


            GetDDL();

            return View(leaveModel);
        }
        [HttpPost]
        public ActionResult LeaveRequest(LeaveModel leaveModel)
        {
            if (_leaveRequestRepository.leave_req_dup_check(leaveModel))
            {
                leaveModel.StaffModel = _timiosStaffRepository.getstaff_by_id(int.Parse(leaveModel.StaffModel.Id));
                leaveModel.LeaveRequestModel.LeaveTypeName = _bizValueRepository.GetSingleById(leaveModel.LeaveRequestModel.LeaveTypeId).Value;
                double numberOfdays = 0;
                DateTime startDate = leaveModel.LeaveRequestModel.StartDate;
                DateTime endDate = leaveModel.LeaveRequestModel.EndDate;

                while (startDate.DayOfYear <= endDate.DayOfYear)
                {
                    if (startDate.DayOfWeek != DayOfWeek.Saturday && startDate.DayOfWeek != DayOfWeek.Sunday)
                    {
                        numberOfdays += 1;
                    }

                    startDate = startDate.AddDays(1);
                }
                if ( (leaveModel.LeaveRequestModel.StartAM ^ leaveModel.LeaveRequestModel.StartPM ) == true )
                {
                    numberOfdays -= 0.5;
                }

                if ((leaveModel.LeaveRequestModel.EndAM ^ leaveModel.LeaveRequestModel.EndPM) == true)
                {
                    numberOfdays -= 0.5;
                }

                leaveModel.LeaveRequestModel.NoOfDays = numberOfdays;
                _leaveRequestRepository.CreateLeaveRequest(leaveModel);

                TimiosStaff staff = _timiosStaffRepository.getstaff_by_id(int.Parse(leaveModel.StaffModel.Id));
                TimiosStaff manager = _timiosStaffRepository.getstaff_by_id(leaveModel.LeaveRequestModel.ManagerInCharge);

                string body = staff.Fullname + " has applied for leave from " + leaveModel.LeaveRequestModel.StartDate + '(' + leaveModel.LeaveRequestModel.StartDate.DayOfWeek.ToString() + ')' + " to " + leaveModel.LeaveRequestModel.EndDate + '(' + leaveModel.LeaveRequestModel.EndDate.DayOfWeek.ToString() + ')';

                //uncomment to send email to manager in charge
                _timiosStaffRepository.create_email(manager.Email, "Leave Application Submission", body);

            }
            else
            {
                leaveModel.StaffModel = GetCurrentUser();
                GetDDL();
                ModelState.AddModelError("LeaveRequestModel.StartDate", "No date overlap or duplication record allowed");
                return View(leaveModel);
            }

            if (leaveModel.LeaveRequestModel.StartDate > leaveModel.LeaveRequestModel.EndDate)
            {
                leaveModel.StaffModel = GetCurrentUser();
                GetDDL();
                ModelState.AddModelError("LeaveRequestModel.StartDate", "invalid date");
                return View(leaveModel);

            }

            return RedirectToAction("Index", "LeaveRequest");
        }

        public ActionResult EditLeaveRequest(int? id)
        {
            if (id > 0)
            {
                LeaveModel leaveModel = new LeaveModel();
                DateTime currentYear = DateTime.ParseExact(DateTime.Now.Year.ToString(), "yyyy", System.Globalization.CultureInfo.InvariantCulture);
             
                LeaveRequest leaveRequest = _leaveRequestRepository.GetLeaveRequestByID((int)id);

                if (leaveRequest.Email != User.Identity.Name)
                {
                    return RedirectToAction("Index", "LeaveRequest");
                }

                leaveModel.LeaveRequestModel = leaveRequest;
                leaveModel.StaffModel = GetCurrentUser();
                GetDDL();

                return View(leaveModel);
            }
            else
            {
                return RedirectToAction("Index", "LeaveRequest");
            }
        }

        [HttpPost]
        public ActionResult EditLeaveRequest(LeaveModel leaveModel)
        {
            leaveModel.StaffModel = GetCurrentUser();
            leaveModel.LeaveRequestModel.LeaveTypeName = _bizValueRepository.GetSingleById(leaveModel.LeaveRequestModel.LeaveTypeId).Value;
            
            double numberOfdays = 0;
            DateTime startDate = leaveModel.LeaveRequestModel.StartDate;
            DateTime endDate = leaveModel.LeaveRequestModel.EndDate;

            while (startDate.DayOfYear <= endDate.DayOfYear)
            {
                if (startDate.DayOfWeek != DayOfWeek.Saturday && startDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    numberOfdays += 1;
                }

                startDate = startDate.AddDays(1);
            }
            if ((leaveModel.LeaveRequestModel.StartAM ^ leaveModel.LeaveRequestModel.StartPM) == true)
            {
                numberOfdays -= 0.5;
            }

            if ((leaveModel.LeaveRequestModel.EndAM ^ leaveModel.LeaveRequestModel.EndPM) == true)
            {
                numberOfdays -= 0.5;
            }

            leaveModel.LeaveRequestModel.NoOfDays = numberOfdays;

            _leaveRequestRepository.UpdateLeaveRequest(leaveModel, false);

            return RedirectToAction("Index", "LeaveRequest");
        }

        public ActionResult ReviewLeaveRequest(int? id)
        {
            if (id > 0)
            {
                LeaveModel leaveModel = new LeaveModel();
                DateTime currentYear = DateTime.ParseExact(DateTime.Now.Year.ToString(), "yyyy", System.Globalization.CultureInfo.InvariantCulture);

                LeaveRequest leaveRequest = _leaveRequestRepository.GetLeaveRequestByID((int)id);
                LeaveBalance leaveBalance = _leaveBalanceRepository.GetLeaveBalanceByStaffIDYear(GetCurrentUser(), currentYear, create_default_leaveBalance());

                leaveModel.LeaveRequestModel = leaveRequest;
                leaveModel.LeaveBalanceModel = leaveBalance;

                GetDDL();

                return View(leaveModel);
            }
            else
            {
                return RedirectToAction("Index", "LeaveRequest");
            }
        }

        [HttpGet]
        public double GetNumberOfAvailableDays(string stringStartDate, string stringEndDate, bool startAM, bool startPM, bool endAM, bool endPM ,string staffId, string leaveTypeId)
        {
            TimiosStaff staff = _timiosStaffRepository.getstaff_by_id(int.Parse(staffId));
            LeaveBalance leaveBalance = _leaveBalanceRepository.GetLeaveBalanceByStaffIDYear(staff, DateTime.Now.Year);
            DateTime startDate = Convert.ToDateTime(stringStartDate);
            DateTime endDate = Convert.ToDateTime(stringEndDate);
            double numberOfDays = 0;

            while (startDate.DayOfYear <= endDate.DayOfYear)
            {
                if (startDate.DayOfWeek != DayOfWeek.Saturday && startDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    numberOfDays += 1;
                }

                startDate = startDate.AddDays(1);
            }
            if ((startAM ^ startPM) == true)
            {
                numberOfDays -= 0.5;
            }

            if ((endAM ^ endPM) == true)
            {
                numberOfDays -= 0.5;
            }

            double numberOfLeftDays = 0;
            if (int.Parse(leaveTypeId) == 6)
            {
                return 0;
            }

            if (int.Parse(leaveTypeId) == 1)
            {
                numberOfLeftDays = leaveBalance.TotalAL - (leaveBalance.TakenAL + numberOfDays);
            }
            else if (int.Parse(leaveTypeId) == 4)
            {
                numberOfLeftDays = leaveBalance.TotalML - (leaveBalance.TakenML + numberOfDays);
            }
            else if (int.Parse(leaveTypeId) == 5)
            {
                numberOfLeftDays = leaveBalance.TotalMPL - (leaveBalance.TakenMPL + numberOfDays);
            }
            else if (int.Parse(leaveTypeId) == 7)
            {
                numberOfLeftDays = leaveBalance.TotalCL - (leaveBalance.TakenCL + numberOfDays);
            }

            return numberOfLeftDays;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReviewLeaveRequest(LeaveModel leaveModel)
        {
            leaveModel.StaffModel = GetCurrentUser();
            TimiosStaff staff = _timiosStaffRepository.getstaff_by_id(int.Parse(leaveModel.LeaveRequestModel.TimiosStaff.Id));
            leaveModel.LeaveRequestModel.LeaveTypeName = _bizValueRepository.GetSingleById(leaveModel.LeaveRequestModel.LeaveTypeId).Value;
            string body = staff.Fullname + " leave application from " + leaveModel.LeaveRequestModel.StartDate + '(' + leaveModel.LeaveRequestModel.StartDate.DayOfWeek.ToString() + ')' + " to " + leaveModel.LeaveRequestModel.EndDate + '(' + leaveModel.LeaveRequestModel.EndDate.DayOfWeek.ToString() + ") has been " + leaveModel.LeaveRequestModel.LeaveStatus + " by " + leaveModel.StaffModel.Fullname;

            if (leaveModel.LeaveRequestModel.LeaveStatus.Equals("Rejected"))
            {
                _leaveRequestRepository.UpdateLeaveRequest(leaveModel, true);
            }
            else if (leaveModel.LeaveRequestModel.LeaveStatus.Equals("Approved"))
            {
                _leaveRequestRepository.UpdateLeaveRequest(leaveModel, true);


                if (leaveModel.LeaveRequestModel.LeaveTypeId == 1)
                {
                    double newBalAL = leaveModel.LeaveBalanceModel.BalanceAL - leaveModel.LeaveRequestModel.NoOfDays;
                    double newTakenAL = leaveModel.LeaveBalanceModel.TakenAL + leaveModel.LeaveRequestModel.NoOfDays;

                    // '1' to update AL
                    _leaveBalanceRepository.UpdateLeaveBalance(1, leaveModel.LeaveBalanceModel.Id, newTakenAL, newBalAL, leaveModel.StaffModel.Fullname);
                }
                else if (leaveModel.LeaveRequestModel.LeaveTypeId == 4)
                {
                    double newBalML = leaveModel.LeaveBalanceModel.BalanceML - leaveModel.LeaveRequestModel.NoOfDays;
                    double newTakenML = leaveModel.LeaveBalanceModel.TakenML + leaveModel.LeaveRequestModel.NoOfDays;

                    // '2' to update ML
                    _leaveBalanceRepository.UpdateLeaveBalance(2, leaveModel.LeaveBalanceModel.Id, newTakenML, newBalML, leaveModel.StaffModel.Fullname);
                }
                else if (leaveModel.LeaveRequestModel.LeaveTypeId == 5)
                {
                    double newBalMPL = leaveModel.LeaveBalanceModel.BalanceMPL - leaveModel.LeaveRequestModel.NoOfDays;
                    double newTakenMPL = leaveModel.LeaveBalanceModel.TakenMPL + leaveModel.LeaveRequestModel.NoOfDays;

                    // '3' to update MPL
                    _leaveBalanceRepository.UpdateLeaveBalance(3, leaveModel.LeaveBalanceModel.Id, newBalMPL, newTakenMPL, leaveModel.StaffModel.Fullname);
                }
                else if (leaveModel.LeaveRequestModel.LeaveTypeId == 7)
                {
                    double newBalCL = leaveModel.LeaveBalanceModel.BalanceCL - leaveModel.LeaveRequestModel.NoOfDays;
                    double newTakenCL = leaveModel.LeaveBalanceModel.TakenCL + leaveModel.LeaveRequestModel.NoOfDays;

                    // '3' to update CL
                    _leaveBalanceRepository.UpdateLeaveBalance(4, leaveModel.LeaveBalanceModel.Id, newBalCL, newTakenCL, leaveModel.StaffModel.Fullname);
                }

                //getting leave request 
                _leaveRequestRepository.SyncLeaveApp();
            }

            _timiosStaffRepository.create_email(staff.Email, "Outcome for leave application", body);


            return RedirectToAction("Index", "LeaveRequest");
        }

        public ActionResult No_Staff()
        {
            return View();
        }


        private void GetYears()
        {
            List<int> Years = new List<int>();
            DateTime startYear = DateTime.Parse("2017-01-01");

            while (startYear.Year <= DateTime.Now.AddYears(1).Year)
            {
                Years.Add(startYear.Year);
                startYear = startYear.AddYears(1);
            }

            ViewBag.Years = Years;
        }
        private void GetDDL()
        {
            List<BizValue> leaveTypeList = _bizValueRepository.GetLeaveTypeList();
            List<TimiosStaff> staffList = _timiosStaffRepository.GetStaffList();

            SelectItems itemsList = new SelectItems();

            if (leaveTypeList != null)
            {
                foreach (var leaveType in leaveTypeList)
                {
                    itemsList.LeaveTypeListItem.Add(new SelectListItem() { Text = leaveType.Value, Value = leaveType.Id.ToString() });
                }
            }
            if (staffList != null)
            {
                foreach (var staff in staffList)
                {
                    itemsList.StaffListItem.Add(new SelectListItem() { Text = staff.Fullname, Value = staff.Id.ToString() });
                }
            }

            ViewBag.LeaveTypes = itemsList.LeaveTypeListItem;
            ViewBag.Staffs = itemsList.StaffListItem;
        }

        private TimiosStaff GetCurrentUser()
        {
            var currentUser = _timiosStaffRepository.GetStaffByEmail(User.Identity.Name);

            return currentUser;
        }
    }
}
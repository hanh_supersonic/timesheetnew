﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize]
    public class CalendarController : Controller
    {
        readonly IAttendanceReportRepository _attendanceReportRepository;
        readonly ILeaveRequestRepository _leaveRequestRepository;

        public CalendarController(IAttendanceReportRepository attendanceReportRepository, ILeaveRequestRepository leaveRequestRepository)
        {
            _attendanceReportRepository = attendanceReportRepository;
            _leaveRequestRepository = leaveRequestRepository;
        }
        public IActionResult Index()
        {
            List<CalendarNote> all_event = _attendanceReportRepository.GetCalendarNotes().ToList();
            _leaveRequestRepository.SubmittedLeave(all_event);
            ViewData["Note"] = all_event;
            return View();
        }
    }
}
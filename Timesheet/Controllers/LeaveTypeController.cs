﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize(Roles = "admin")]
    public class LeaveTypeController : Controller
    {
        readonly IBizValueRepository _bizValueRepository;
        readonly UserManager<ApplicationUser> _userManager;
        readonly IActivityRepository _activityRepository;
        public LeaveTypeController(IBizValueRepository bizValueRepository, UserManager<ApplicationUser> userManager,
                                IActivityRepository activityRepository)

        {
            _bizValueRepository = bizValueRepository;
            _userManager = userManager;
            _activityRepository = activityRepository;
        }

        public IActionResult Index()
        {
            var bizValues = _bizValueRepository.GetAll().ToList();
            return View(bizValues);
        }

        [HttpGet]
        public BizValue GetInforLeave(int id)
        {
            return _bizValueRepository.GetSingleById(id);
        }

        [HttpPost]
        public async Task<IActionResult> AddLeave(BizValueViewModel bizValueViewModel)
        {
            if (true)
            {
                return RedirectToAction("Index", "LeaveType");
            }
            var currentUser = this.User;
            var user = await _userManager.GetUserAsync(currentUser);

            BizValue bizValue = new BizValue();
            bizValue.IsDeleted = false;
            bizValue.Type = "LT";
            bizValue.Value = bizValueViewModel.Value;
            bizValue.SortOrder = bizValueViewModel.SortOrder;
            bizValue.LastUpdatedBy = user;
            bizValue.LastUpdatedOn = DateTime.Now;
            _bizValueRepository.Add(bizValue);

            Activity activity = new Activity()
            {
                ApplicationUser = user,
                Icon = "fas fa-bed",
                CreatedAt = DateTime.Now,
                Content = "Add new leave",
            };
            _activityRepository.Add(activity);

            return RedirectToAction("Index", "LeaveType");
        }

        public async Task<IActionResult> UpdateLeave(BizValueViewModel bizValueViewModel)
        {
            if (true)
            {
                return RedirectToAction("Index", "LeaveType");
            }
            var currentUser = this.User;
            var user = await _userManager.GetUserAsync(currentUser);

            BizValue bizValue = new BizValue();
            bizValue.Id = bizValueViewModel.Id;
            bizValue.Value = bizValueViewModel.Value;
            bizValue.SortOrder = bizValueViewModel.SortOrder;
            bizValue.LastUpdatedBy = user;
            bizValue.LastUpdatedOn = DateTime.Now;
            _bizValueRepository.Update(bizValue);

            Activity activity = new Activity()
            {
                ApplicationUser = user,
                Icon = "fas fa-bed",
                CreatedAt = DateTime.Now,
                Content = "Update a leave",
            };
            _activityRepository.Add(activity);

            return RedirectToAction("Index", "LeaveType");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteLeave(int id)
        {
            if (true)
            {
                return RedirectToAction("Index", "LeaveType");
            }
            var currentUser = this.User;
            var user = await _userManager.GetUserAsync(currentUser);

            _bizValueRepository.Delete(id);

            Activity activity = new Activity()
            {
                ApplicationUser = user,
                Icon = "fas fa-bed",
                CreatedAt = DateTime.Now,
                Content = "Delete a leave",
            };
            _activityRepository.Add(activity);

            return RedirectToAction("Index", "LeaveType");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Timesheet.Data;
using Timesheet.Helper;
using Timesheet.Models;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize(Roles = "admin")]
    //[AllowAnonymous]
    public class ProjectController : Controller
    {
        readonly IEffortDefaultRepository _effortDefaultRepository;
        readonly IActivityDefaultRepository _activityDefaultRepository;
        readonly IProjectRepository _projectRepository;
        readonly IProjectConfigRepository _projectConfigRepository;
        readonly IEffortRepository _effortRepository;
        readonly IProjectActivityRepository _projectActivityRepository;
        readonly IUserRepository _userRepository;
        readonly IRoleRepository _roleRepository;
        readonly IPositionRepository _positionRepository;
        RoleManager<IdentityRole> _roleManager;
        readonly IProjectMemberRepository _projectMemberRepository;
        readonly UserManager<ApplicationUser> _userManager;
        readonly IActivityRepository _activityRepository;
        public ProjectController(IEffortDefaultRepository effortDefaultRepository, 
                                IActivityDefaultRepository activityDefaultRepository,
                                IProjectRepository projectRepository, IProjectConfigRepository projectConfigRepository,
                                IEffortRepository effortRepository, IProjectActivityRepository projectActivityRepository,
                                IUserRepository userRepository, IPositionRepository positionRepository,
                                RoleManager<IdentityRole> roleManager, IRoleRepository roleRepository,
                                IProjectMemberRepository projectMemberRepository, UserManager<ApplicationUser> userManager,
                                IActivityRepository activityRepository)
        {
            _effortDefaultRepository = effortDefaultRepository;
            _activityDefaultRepository = activityDefaultRepository;
            _projectRepository = projectRepository;
            _projectConfigRepository = projectConfigRepository;
            _effortRepository = effortRepository;
            _projectActivityRepository = projectActivityRepository;
            _positionRepository = positionRepository;
            _userRepository = userRepository;
            _roleManager = roleManager;
            _roleRepository = roleRepository;
            _projectMemberRepository = projectMemberRepository;
            _userManager = userManager;
            _activityRepository = activityRepository;
        }

        public IActionResult Index()
        {
            List<ProjectViewModel> projectViewModels = new List<ProjectViewModel>();
            var projects = _projectRepository.GetAll();
            foreach(var project in projects)
            {
                ProjectViewModel projectViewModel = new ProjectViewModel();
                projectViewModel.Id = project.Id;
                projectViewModel.Name = project.Name;
                projectViewModel.QuotationNumber = project.QuotationNumber;
                projectViewModel.OrderNumber = project.OrderNumber;
                projectViewModel.Company = project.Company;
                projectViewModel.Description = project.Description;

                ProjectConfig projectConfig = _projectConfigRepository.GetSingleByProject(project.Id);
                projectViewModel.PlannedEnd = projectConfig.PlannedEnd;
                projectViewModel.PlannedStart = projectConfig.PlannedStart;
                projectViewModel.Status = projectConfig.Status;

                projectViewModels.Add(projectViewModel);
            }
            

            return View(projectViewModels);
        }

        public IActionResult Detail(string id)
        {
            Timesheet.Data.Project project = _projectRepository.GetSingleById(id);
            ProjectConfig projectConfig = _projectConfigRepository.GetSingleByProject(project.Id);
            ProjectActivity projectActivity = _projectActivityRepository.GetSingleByConfig(projectConfig.Id);

            var efforts = _effortRepository.GetManyByActivity(projectActivity.Id).ToList();
            foreach (var effort in efforts)
            {
                effort.StageName = effort.StageName.UpperFirstLetter();
            }
            ViewBag.Efforts = efforts;
            ViewBag.ActivityDefaults = _activityDefaultRepository.GetAll();
            ViewBag.Project = project;
            ViewBag.ProjectConfig = projectConfig;
            ViewBag.ProjectActivity = projectActivity;
            ViewBag.Positions = _positionRepository.GetAll();

            var role = _roleRepository.GetByName("member");
            ViewBag.Members = _userRepository.GetByRole(role.Id).Where(x => x.IsDeleted == false).ToList();
            ViewBag.Positions = _positionRepository.GetAll();

            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            var effortDefaults = _effortDefaultRepository.GetAll().OrderBy(x => x.Order);
            foreach (var effortDefault in effortDefaults)
            {
                effortDefault.Name = effortDefault.Name.UpperFirstLetter();
            }
            ViewBag.EffortDefaults = effortDefaults;
            ViewBag.ActivityDefaults = _activityDefaultRepository.GetAll();
            ViewBag.Positions = _positionRepository.GetAll();

            var role = _roleRepository.GetByName("member");
            ViewBag.Members = _userRepository.GetByRole(role.Id).Where(x => x.IsDeleted == false).ToList();
            ViewBag.Positions = _positionRepository.GetAll();


            return View();
        }


        [HttpPost]
        public async Task<string> CreateStageOne(StageOneViewModel stageOneViewModel)
        {
            var currentUser = this.User;
            var user = await _userManager.GetUserAsync(currentUser);

            Timesheet.Data.Project project = new Timesheet.Data.Project();
            project.Id = stageOneViewModel.Id;
            project.Name = stageOneViewModel.Name;
            project.OrderNumber = stageOneViewModel.OrderNumber;
            project.QuotationNumber = stageOneViewModel.QuotationNumber;
            project.Company = stageOneViewModel.Company;
            project.Description = stageOneViewModel.Description;
            project.CreatedAt = DateTime.Now.ToString();
            project.CreatedBy = user;

            ProjectConfig projectConfig = new ProjectConfig();
            projectConfig.Project = project;
            projectConfig.Status = stageOneViewModel.Status;
            projectConfig.CreatedDate = DateTime.Now.ToString();
            projectConfig.IsUsed = true;
            projectConfig.IsDeleted = false;
            projectConfig.HourUltilized = stageOneViewModel.HourUltilized;
            projectConfig.PlannedStart = stageOneViewModel.PlannedStart;
            projectConfig.PlannedEnd = stageOneViewModel.PlannedEnd;

            ProjectActivity projectActivity = new ProjectActivity();
            projectActivity.IsDeleted = false;
            projectActivity.Name = _activityDefaultRepository.GetSingleById(int.Parse(stageOneViewModel.Activity)).Name;
            projectActivity.ProjectConfig = projectConfig;


            _projectRepository.Add(project);
            _projectConfigRepository.Add(projectConfig);
            _projectActivityRepository.Add(projectActivity);

            var effortDefaults = _effortDefaultRepository.GetAll().OrderBy(x => x.Order).ToList();
            for (int i = 1; i < stageOneViewModel.Efforts.Count; i++)
            {
                Effort effort = new Effort();
                effort.IsDeleted = false;
                effort.ProjectActivity = projectActivity;
                effort.StageName = effortDefaults[i - 1].Name;
                effort.EffortNumber = stageOneViewModel.Efforts[i];
                effort.CreatedAt = DateTime.Now.ToString();

                _effortRepository.Add(effort);
            }

            Activity activity = new Activity()
            {
                ApplicationUser = user,
                Icon = "fas fa-th",
                CreatedAt = DateTime.Now,
                Content = "Create new project",
            };
            _activityRepository.Add(activity);
            

            return project.Id;
        }

        [HttpPost]
        public List<ProjectMemberViewModel> CreateStageTwo(CreateStageTwoViewModel createStageTwoViewModel)
        {
            if (createStageTwoViewModel.Positions == null)
            {
                return null;
            }
            foreach (var positionId in createStageTwoViewModel.Positions)
            {
                var position = _positionRepository.GetSingleById(positionId);
                var project = _projectRepository.GetSingleById(createStageTwoViewModel.ProjectId);
                var member = _userRepository.GetSingleById(createStageTwoViewModel.MemberId);

                if (_projectMemberRepository.IsIn(createStageTwoViewModel.ProjectId, createStageTwoViewModel.MemberId, positionId))
                {
                    continue;
                }

                ProjectMember projectMember = new ProjectMember();
                projectMember.Position = position;
                projectMember.Project = project;
                projectMember.ApplicationUser = member;

                _projectMemberRepository.Add(projectMember);
            }

            var projectMemberGroups = _projectMemberRepository.GetManyByProjectId(createStageTwoViewModel.ProjectId, new string[] { "ApplicationUser", "Project", "Position" })
                            .GroupBy(x => x.ApplicationUser).ToList();

            List<ProjectMemberViewModel> projectMemberViewModels = new List<ProjectMemberViewModel>();
            foreach (var projectMembers in projectMemberGroups)
            {
                ProjectMemberViewModel projectMemberViewModel = new ProjectMemberViewModel();
                projectMemberViewModel.ApplicationUser = projectMembers.Key;
                projectMemberViewModel.ProjectId = createStageTwoViewModel.ProjectId;
                projectMemberViewModel.Positions = projectMembers.Select(x => x.Position).OrderBy(x => x.Id).ToList();

                projectMemberViewModels.Add(projectMemberViewModel);
            }

            var currentUser = this.User;
            var currentUserId = _userManager.GetUserId(currentUser);
            ApplicationUser user = _userRepository.GetSingleById(currentUserId);

            Activity activity = new Activity()
            {
                ApplicationUser = user,
                Icon = "far fa-user",
                CreatedAt = DateTime.Now,
                Content = "Update member in project",
            };
            _activityRepository.Add(activity);

            return projectMemberViewModels;
        }

        [HttpGet]
        public List<ProjectMemberViewModel> GetMemberOfProject(string projectId)
        {
            var projectMemberGroups = _projectMemberRepository.GetManyByProjectId(projectId, new string[] { "ApplicationUser", "Project", "Position"})
                            .GroupBy(x => x.ApplicationUser).ToList();

            List<ProjectMemberViewModel> projectMemberViewModels = new List<ProjectMemberViewModel>();
            foreach(var projectMembers in projectMemberGroups)
            {
                ProjectMemberViewModel projectMemberViewModel = new ProjectMemberViewModel();
                projectMemberViewModel.ApplicationUser = projectMembers.Key;
                projectMemberViewModel.ProjectId = projectId;
                projectMemberViewModel.Positions = projectMembers.Select(x => x.Position).OrderBy(x => x.Id).ToList();

                projectMemberViewModels.Add(projectMemberViewModel);
            }
            return projectMemberViewModels;
        }

        [HttpPost]
        public List<ProjectMemberViewModel> UpdatePositionOfMember(CreateStageTwoViewModel createStageTwoViewModel)
        {
            if (createStageTwoViewModel.Positions == null)
            {
                return null;
            }
            foreach (var positionId in createStageTwoViewModel.Positions)
            {
                var position = _positionRepository.GetSingleById(positionId);
                var project = _projectRepository.GetSingleById(createStageTwoViewModel.ProjectId);
                var member = _userRepository.GetSingleById(createStageTwoViewModel.MemberId);

                _projectMemberRepository.Delete(createStageTwoViewModel.ProjectId, createStageTwoViewModel.MemberId, positionId);
            }

            var projectMemberGroups = _projectMemberRepository.GetManyByProjectId(createStageTwoViewModel.ProjectId, new string[] { "ApplicationUser", "Project", "Position" })
                            .GroupBy(x => x.ApplicationUser).ToList();

            List<ProjectMemberViewModel> projectMemberViewModels = new List<ProjectMemberViewModel>();
            foreach (var projectMembers in projectMemberGroups)
            {
                ProjectMemberViewModel projectMemberViewModel = new ProjectMemberViewModel();
                projectMemberViewModel.ApplicationUser = projectMembers.Key;
                projectMemberViewModel.ProjectId = createStageTwoViewModel.ProjectId;
                projectMemberViewModel.Positions = projectMembers.Select(x => x.Position).OrderBy(x => x.Id).ToList();

                projectMemberViewModels.Add(projectMemberViewModel);
            }

            var currentUser = this.User;
            var currentUserId = _userManager.GetUserId(currentUser);
            ApplicationUser user = _userRepository.GetSingleById(currentUserId);

            Activity activity = new Activity()
            {
                ApplicationUser = user,
                Icon = "fas fa-th",
                CreatedAt = DateTime.Now,
                Content = "Update member in project",
            };
            _activityRepository.Add(activity);

            return projectMemberViewModels;
        }

        [HttpPost]
        public IActionResult Update(StageOneViewModel stageOneViewModel)
        {
            var project = _projectRepository.GetSingleById(stageOneViewModel.Id);
            project.Name = stageOneViewModel.Name;
            project.OrderNumber = stageOneViewModel.OrderNumber;
            project.QuotationNumber = stageOneViewModel.QuotationNumber;
            project.Company = stageOneViewModel.Company;
            project.Description = stageOneViewModel.Description;
            _projectRepository.Update(project);

            var projectConfig = _projectConfigRepository.GetSingleByProject(project.Id);
            projectConfig.Status = stageOneViewModel.Status;
            projectConfig.HourUltilized = stageOneViewModel.HourUltilized;
            projectConfig.PlannedStart = stageOneViewModel.PlannedStart;
            projectConfig.PlannedEnd = stageOneViewModel.PlannedEnd;
            _projectConfigRepository.Update(projectConfig);

            var projectActivity = _projectActivityRepository.GetSingleByConfig(projectConfig.Id);
            projectActivity.Name = _activityDefaultRepository.GetSingleById(int.Parse(stageOneViewModel.Activity)).Name;
            _projectActivityRepository.Update(projectActivity);

            var efforts = _effortRepository.GetManyByActivity(projectActivity.Id).ToList();
            for(int i = 0; i < efforts.Count(); i++)
            {
                efforts[i].EffortNumber = stageOneViewModel.Efforts[i+1];
                _effortRepository.Update(efforts[i]);
            }

            var currentUser = this.User;
            var currentUserId = _userManager.GetUserId(currentUser);
            ApplicationUser user = _userRepository.GetSingleById(currentUserId);

            Activity activity = new Activity()
            {
                ApplicationUser = user,
                Icon = "fas fa-th",
                CreatedAt = DateTime.Now,
                Content = "Update information of project",
            };
            _activityRepository.Add(activity);

            return Ok();
        }

        


    }
}

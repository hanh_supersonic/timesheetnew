﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Models;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize(Roles = "admin")]
    public class AttendController : Controller
    {
        readonly UserManager<ApplicationUser> _userManager;
        readonly IActivityRepository _activityRepository;
        readonly ILeaveRequestRepository _leaveRequestRepository;
        readonly IAttendanceReportRepository _attendanceReportRepository;
        readonly IUserRepository _userRepository;
        readonly ILeaveBalanceConfigRepository _leaveBalanceConfigRepository;
        readonly ITimiosStaffRepository _timiosStaffRepository;

        public AttendController(UserManager<ApplicationUser> userManager, IActivityRepository activityRepository,
                                ILeaveRequestRepository leaveRequestRepository, IAttendanceReportRepository attendanceReportRepository,
                                IUserRepository userRepository, ILeaveBalanceConfigRepository leaveBalanceConfigRepository, 
                                ITimiosStaffRepository timiosStaffRepository)
        {
            _userManager = userManager;
            _activityRepository = activityRepository;
            _leaveRequestRepository = leaveRequestRepository;
            _attendanceReportRepository = attendanceReportRepository;
            _userRepository = userRepository;
            _leaveBalanceConfigRepository = leaveBalanceConfigRepository;
            _timiosStaffRepository = timiosStaffRepository;
        }

        public ActionResult Index()
        {

            _leaveRequestRepository.SyncLeaveApp();

            var all_log = _attendanceReportRepository.GetAttendanceLog().ToList();


            ViewData["Absent"] = _attendanceReportRepository.query_status("Absent", all_log);
            ViewData["Medical Leave"] = _attendanceReportRepository.query_status("Medical Leave", all_log);
            ViewData["Paid Leave"] = _attendanceReportRepository.query_status("Paid Leave", all_log);
            ViewData["Unpaid Leave"] = _attendanceReportRepository.query_status("Unpaid Leave", all_log);
            ViewData["Others"] = _attendanceReportRepository.query_status("Others", all_log);
            ViewData["Maternity/Paternity Leave"] = _attendanceReportRepository.query_status("Maternity/Paternity", all_log);
            ViewData["Childcare Leave"] = _attendanceReportRepository.query_status("Childcare", all_log);
            ViewData["Annual Leave"] = _attendanceReportRepository.query_status("Annual Leave", all_log);

            ViewBag.Statuses = _attendanceReportRepository.status_list();
            ViewBag.Names = _attendanceReportRepository.getnamelist();

            return View(all_log);
        }

        //getting chart for everyone only accessible by administrator
        //[ActionName("Detail")]
        //[RequireHttps]
        //public ActionResult Detail()
        //{
        //    int year;
        //    string status;
        //    List<AttendanceReport> all_log = _attendanceReportRepository.GetAttendanceLog().ToList();
        //    string month = string.Empty;
        //    string team_name = Request.Form["Team"];



        //    if (String.IsNullOrEmpty(Request.Form["month"]) || Request.Form["month"] == "")
        //    {
        //        month = "Jan";
        //    }
        //    else
        //    {
        //        month = Request.Form["month"];
        //    }
        //    if (String.IsNullOrEmpty(Request.Form["Status"]) && Request.Form["Status"] != "")
        //    {
        //        status = Request.Form["Status"];

        //    }
        //    else
        //    {
        //        status = "Absent";

        //    }

        //    if (String.IsNullOrEmpty(Request.Form["year"]) || Request.Form["year"] == "")
        //    {
        //        year = DateTime.Now.Year;
        //    }
        //    else
        //    {
        //        year = Convert.ToInt32(Request.Form["year"]);
        //    }


        //Count for all log  from today back to 30 days ago
        //all_log = all_log.Where(s => s.Timestamp.ToString("MMM") == month).ToList();
        //all_log = all_log.Where(s => s.Timestamp.Year == year).ToList();
        //if (String.IsNullOrEmpty(Request.Form["Team"]) && Request.Form["Team"] != "")
        //{
        //    all_log = Team_Utility.team_query(all_log, Request.Form["Team"]);
        //}

        //if (Team_Utility.check_team_leader(User.Identity.Name))
        //{
        //    //all_log = Attendance_Utility.team_report(all_log, User.Identity.Name);
        //    team_name = Team_Utility.get_team_by_lead_email(User.Identity.Name);
        //}


        //List<ChartData> all_chart = AttendanceReport.GetChartDatas(all_log, team_name, status);
        //ViewBag.DataPoints = all_chart;
        //ViewData["month"] = month;
        //ViewData["Team"] = Request.Form["Team"];
        //    return View();
        //}


        [HttpGet]
        public ActionResult Edit(int id = 0)
        {

            AttendanceReport update_log = _attendanceReportRepository.GetSingleById(id);


            return View(update_log);
        }

        [HttpPost]
        public ActionResult MakeChanges()
        {
            _attendanceReportRepository.Update(int.Parse(Request.Form["ID"]), Request.Form["Name"], Request.Form["DeviceName"], Request.Form["Status"], Request.Form["Timestamp"], Request.Form["Remark"]);

            var currentUser = this.User;
            var currentUserId = _userManager.GetUserId(currentUser);
            Activity activity = new Activity()
            {
                ApplicationUser = _userRepository.GetSingleById(currentUserId),
                Icon = "fas fa-bed",
                CreatedAt = DateTime.Now,
                Content = "Update attendance",
            };
            _activityRepository.Add(activity);
            // List <Attendance_report> all_log = DB.Attendance_reports.Where(a => a.ID == Convert.ToInt32(Request.Form["ID"]) && a.Status == "Absent").ToList();
            return RedirectToAction("Index", new { name = Request.Form["Name"] });
        }

    }
}
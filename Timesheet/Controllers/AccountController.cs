﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Models;
using Timesheet.Repository;

using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;
using MimeKit;

namespace Timesheet.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        UserManager<ApplicationUser> _userManager;
        SignInManager<ApplicationUser> _signInManager;
        RoleManager<IdentityRole> _roleManager;
        IUserRepository _userRepository;
        public AccountController(UserManager<ApplicationUser> userManager,
                                SignInManager<ApplicationUser> signInManager,
                                RoleManager<IdentityRole> roleManager, IUserRepository userRepository )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _userRepository = userRepository;
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);
                

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, model.RoleName);
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }

                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            

            LoginViewModel loginViewModel = new LoginViewModel()
            {
                ReturnUrl = returnUrl,
                ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList()
            };

            return View(loginViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            model.ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {

                if(_userRepository.GetSingleByUsername(model.Email) == null || _userRepository.GetSingleByUsername(model.Email).IsDeleted == true)
                {
                    ModelState.AddModelError(string.Empty, "Invalid Login Attemp");
                    return View(model);
                }
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password,
                                                                    true, false);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attemp");
            }
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleViewModel model)
        {
            IdentityRole identityRole = new IdentityRole()
            {
                Name = model.RoleName
            };

            await _roleManager.CreateAsync(identityRole);

            return Ok();
        }

        [HttpPost]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });

            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }
        
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            LoginViewModel loginViewModel = new LoginViewModel()
            {
                ReturnUrl = returnUrl,
                ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList()
            };

            if (remoteError != null)
            {
                ModelState.AddModelError(string.Empty, $"Error from external provider: {remoteError}");
                return View("Login", loginViewModel);
            }

            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ModelState.AddModelError(string.Empty, $"Error loading external login information");
                return View("Login", loginViewModel);
            }

            var signInResult = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey
                                                    , isPersistent: false, bypassTwoFactor: true);

            if (signInResult.Succeeded)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);

                if (email != null)
                {
                    var user = await _userManager.FindByNameAsync(email);

                    if (user == null)
                    {
                        user = new ApplicationUser
                        {
                            UserName = info.Principal.FindFirstValue(ClaimTypes.Email),
                            Email = info.Principal.FindFirstValue(ClaimTypes.Email),
                        };

                        await _userManager.CreateAsync(user);
                        //await _userManager.AddToRoleAsync(user, "admin");
                    }

                    await _userManager.AddLoginAsync(user, info);
                    await _signInManager.SignInAsync(user, isPersistent: false);

                    return LocalRedirect(returnUrl);

                }


            }
            
            return View("Login", loginViewModel);

        }

        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                    var passwordResetLink = Url.Action("ResetPassword", "Account",
                                            new { email = model.Email, token = token }, Request.Scheme);

                    //MailAddress addressFrom = new MailAddress("hanh@supersonic.sg");
                    //MailAddress addressTo = new MailAddress(model.Email);
                    //MailMessage message = new MailMessage(addressFrom, addressTo);

                    //message.Body = @"Click this link to open reset password page<br>"+ passwordResetLink;

                    //SmtpClient client = new SmtpClient();
                    //client.Host = "smtp.sendgrid.com";
                    //client.Port = 587;
                    //client.EnableSsl = false;
                    //client.UseDefaultCredentials = false;
                    //client.Credentials = new NetworkCredential("azure_a1ec7ed1a7de63c31adf4a41a6ea6a11@azure.com", "OLOu43VvTJ647Qu");
                    //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //client.Send(message);
                    create_email(model.Email, "Reset Password", @"Click this link to open reset password page " + passwordResetLink);
                    return View("ForgotPasswordConfirmation");
                }

                return View("ForgotpasswordConfirmation");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult ResetPassword(string token, string email)
        {
            if (token == null || email == null)
            {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            return View();
        }

        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
                    if (result.Succeeded)
                    {
                        return View("ResetPasswordConfirmation");
                    }
                    foreach ( var erorr in result.Errors)
                    {
                        ModelState.AddModelError("", erorr.Description);
                    }

                    return View(model);
                }

                return View("ResetPasswordConfirmation");
            }

            return View(model);
        }

        public void create_email(string email, string subject, string body)
        {
            Microsoft.AspNet.Identity.IdentityMessage IM = new Microsoft.AspNet.Identity.IdentityMessage();
            IM.Destination = email;
            IM.Subject = subject;
            IM.Body = body;
            configSendMailKit(IM);
        }

        public static void configSendMailKit(Microsoft.AspNet.Identity.IdentityMessage message)
        {
            var myMessage = new MimeMessage();
            myMessage.From.Add(new MailboxAddress("Admin", "Admin@timios.net"));
            myMessage.To.Add(new MailboxAddress("User", message.Destination));
            myMessage.Sender = (new MailboxAddress("Admin", "Admin@timios.net"));
            myMessage.Subject = message.Subject;
            myMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = message.Body };

            try
            {
                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("in-v3.mailjet.com", 587, SecureSocketOptions.Auto);

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate("8d179afcd3cdb4c0d8d6314d9bc47fcd", "628a7dcd4f879e58330baa7368475dc3");
                    client.Send(myMessage);
                    var smtp = new SmtpClient(new ProtocolLogger(Console.OpenStandardOutput()));

                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }
        }
    }
}
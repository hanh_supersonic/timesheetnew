﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Models;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize(Roles = "admin")]
    public class DashboardController : Controller
    {
        RoleManager<IdentityRole> _roleManager;
        readonly IUserRepository _userRepository;
        readonly IRoleRepository _roleRepository;
        readonly IActivityRepository _activityRepository;
        readonly IProjectRepository _projectRepository;
        readonly IProjectConfigRepository _projectConfigRepository;
        readonly IEffortRepository _effortRepository;
        readonly ICurrentEffortRepository _currentEffortRepository;
        readonly IProjectActivityRepository _projectActivityRepository;

        public DashboardController(IActivityRepository activityRepository, RoleManager<IdentityRole> roleManager,
                                IUserRepository userRepository, IRoleRepository roleRepository,
                                IProjectRepository projectRepository, IProjectConfigRepository projectConfigRepository,
                                IEffortRepository effortRepository, ICurrentEffortRepository currentEffortRepository,
                                IProjectActivityRepository projectActivityRepository)
        {
            _activityRepository = activityRepository;
            _roleManager = roleManager;
            _userRepository = userRepository;
            _roleManager = roleManager;
            _projectConfigRepository = projectConfigRepository;
            _projectRepository = projectRepository;
            _roleRepository = roleRepository;
            _effortRepository = effortRepository;
            _currentEffortRepository = currentEffortRepository;
            _projectActivityRepository = projectActivityRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Today()
        {
            var activities = _activityRepository.GetByDate(DateTime.Now).OrderByDescending(x => x.CreatedAt).ToList();
            foreach(var activity in activities)
            {
                if (String.IsNullOrEmpty(activity.ApplicationUser.Avatar))
                {
                    activity.ApplicationUser.Avatar = "avatar-1.png";
                }
            }
            return View(activities);
        }

        private string GetRandomColor()
        {
            var letters = "0123456789ABCDEF";
            var color = "#";
            for(int i = 0; i < 6; i++)
            {
                color += letters.ElementAt((new Random()).Next(0, 16));
            }
            return color;
        }

        public IActionResult General()
        {
            var users = _userRepository.GetAll();
            List<MemberViewModel> memberViewModels = new List<MemberViewModel>();
            foreach (var user in users)
            {
                var roles = _roleRepository.GetByUser(user.Id).ToList(); ;
                memberViewModels.Add(new MemberViewModel()
                {
                    User = user,
                    Roles = roles,
                });
            }
            var projects = _projectRepository.GetAll();

            #region status
            int cancelProjectNumber = 0;
            int closedProjectNumber = 0;
            int runningProjectNumber = 0;
            foreach(var project in projects)
            {
                var projectConfig = _projectConfigRepository.GetSingleByProject(project.Id);
                if (projectConfig.Status == 1)
                {
                    cancelProjectNumber += 1;
                }
                else if (projectConfig.Status == 2)
                {
                    closedProjectNumber += 1;
                }
                else if (projectConfig.Status == 3)
                {
                    runningProjectNumber += 1;
                }
            }

            List<int> statusProjectDatas = new List<int>();
            statusProjectDatas.Add(cancelProjectNumber);
            statusProjectDatas.Add(closedProjectNumber);
            statusProjectDatas.Add(runningProjectNumber);

            List<string> statusProjectLabels = new List<string>();
            statusProjectLabels.Add("Cancel");
            statusProjectLabels.Add("Closed");
            statusProjectLabels.Add("Running");

            List<string> statusProjectBackgroundColors = new List<string>();
            statusProjectBackgroundColors.Add("rgba(255, 0, 0, 0.2)");
            statusProjectBackgroundColors.Add("rgba(0, 0, 255, 0.2)");
            statusProjectBackgroundColors.Add("rgba(0, 128, 0, 0.2)");

            List<string> statusProjectBorderColors = new List<string>();
            statusProjectBorderColors.Add("rgba(255, 0, 0, 1)");
            statusProjectBorderColors.Add("rgba(0, 0, 255, 1)");
            statusProjectBorderColors.Add("rgba(0, 128, 0, 1)");
            #endregion

            #region progress
            var projectIds = projects.Select(x => x.Id).ToList();
            var projectNames = projects.Select(x => x.Name).ToList();
            List<double> completedPercentDatas = new List<double>();
            List<string> completedPercentBackgroundColors = new List<string>();
            foreach (var projectId in projectIds)
            {
                var projectConfig = _projectConfigRepository.GetSingleByProject(projectId);
                var projectActivity = _projectActivityRepository.GetSingleByConfig(projectConfig.Id);
                float totalEffort = 0;
                float totalCurrentEffort = 0;
                var efforts = _effortRepository.GetManyByActivity(projectActivity.Id).ToList();

                foreach (var item in efforts)
                {
                    totalEffort += float.Parse(item.EffortNumber);
                }

                var allEfforts = _currentEffortRepository.GetAll(projectId).ToList();
                List<CurrentEffort> allCurrentEfforts = new List<CurrentEffort>();
                foreach (var item in efforts)
                {

                    float number = 0;
                    var thisEfforts = allEfforts.Where(x => x.Effort.Id == item.Id).ToList();
                    foreach (var thisEffort in thisEfforts)
                    {
                        number += float.Parse(thisEffort.EffortNumber);
                    }
                    totalCurrentEffort += number;
                }

                completedPercentDatas.Add(Math.Round(totalCurrentEffort * 100 / totalEffort));
                completedPercentBackgroundColors.Add(GetRandomColor());
            }


            #endregion

            //ViewBag
            ViewBag.ProjectNumber = projects.Count();
            ViewBag.StatusProjectDatas = statusProjectDatas;
            ViewBag.StatusProjectLabels = statusProjectLabels;
            ViewBag.StatusProjectBackgroundColors = statusProjectBackgroundColors;
            ViewBag.StatusProjectBorderColors = statusProjectBorderColors;
            ViewBag.MemberNumber = memberViewModels.Count();
            ViewBag.AdminNumber = memberViewModels.Where(x => x.Roles.Any(r => r.Name == "admin") == true).Count();

            ViewBag.CompletedPercentDatas = completedPercentDatas;
            ViewBag.CompletedPercentLabels = projectNames;
            ViewBag.CompletedPercentBackgroundColors = completedPercentBackgroundColors;

            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Models;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        readonly UserManager<ApplicationUser> _userManager;
        readonly IUserRepository _userRepository;
        [Obsolete]
        readonly IHostingEnvironment _hostingEnvironment;
        readonly ITimiosStaffRepository _timiosStaffRepository;

        [Obsolete]
        public ProfileController(UserManager<ApplicationUser> userManager, IUserRepository userRepository,
                                IHostingEnvironment hostingEnvironment, ITimiosStaffRepository timiosStaffRepository)
        {
            _userManager = userManager;
            _userRepository = userRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var currentUser = this.User;
            var user = await _userManager.GetUserAsync(currentUser);
            if (user.FullName == null)
            {
                user.FullName = "Blank";
            }

            if (user.Avatar == null || user.Avatar == "")
            {
                user.Avatar = "avatar-1.png";
            }


            return View(user);
        }

        [HttpPost]
        [Obsolete]
        public IActionResult UpdateProfile(ProfileViewModel profileViewModel)
        {
            if (profileViewModel.Phone == null)
            {
                profileViewModel.Phone = "";
            }
            

            var currentUser = this.User;
            var currentUserId = _userManager.GetUserId(currentUser);
            ApplicationUser user = _userRepository.GetSingleById(currentUserId);

            string uniqueFileName = null;
            if (profileViewModel.Avatar != null && profileViewModel.Avatar.FileName != "")
            {
                if (profileViewModel.Avatar.FileName != user.Avatar)
                {
                    string uploadsFoldear = Path.Combine(_hostingEnvironment.WebRootPath, "img/avatar");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + profileViewModel.Avatar.FileName;
                    string filePath = Path.Combine(uploadsFoldear, uniqueFileName);
                    profileViewModel.Avatar.CopyTo(new FileStream(filePath, FileMode.Create));
                    user.Avatar = uniqueFileName;
                }

            }
            user.PhoneNumber = profileViewModel.Phone;
            user.FullName = profileViewModel.Fullname;
            _userRepository.UpdateProfile(user);

            var timiosStaff = _timiosStaffRepository.GetByUserId(user.Id);
            timiosStaff.Contact = user.PhoneNumber;
            timiosStaff.Email = user.Email;
            timiosStaff.Fullname = user.FullName;

            _timiosStaffRepository.UpdateInformation(timiosStaff);


            return RedirectToAction("Index", "Profile");
        }
    }
}
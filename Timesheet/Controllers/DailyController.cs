﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Timesheet.Data;
using Timesheet.Helper;
using Timesheet.Models;
using Timesheet.Repository;

namespace Timesheet.Controllers
{
    [Authorize]
    //[AllowAnonymous]
    public class DailyController : Controller
    {
        readonly IEffortDefaultRepository _effortDefaultRepository;
        readonly IActivityDefaultRepository _activityDefaultRepository;
        readonly IProjectRepository _projectRepository;
        readonly IProjectMemberRepository _projectMemberRepository;
        readonly IProjectConfigRepository _projectConfigRepository;
        readonly IEffortRepository _effortRepository;
        readonly IProjectActivityRepository _projectActivityRepository;
        readonly ICurrentEffortRepository _currentEffortRepository;
        readonly UserManager<ApplicationUser> _userManager;
        readonly IUserRepository _userRepository;
        readonly IActivityRepository _activityRepository;

        public DailyController(IEffortDefaultRepository effortDefaultRepository,
                                IActivityDefaultRepository activityDefaultRepository,
                                IProjectRepository projectRepository, IProjectConfigRepository projectConfigRepository,
                                IEffortRepository effortRepository, IProjectActivityRepository projectActivityRepository,
                                ICurrentEffortRepository currentEffortRepository, UserManager<ApplicationUser> userManager,
                                IProjectMemberRepository projectMemberRepository, IUserRepository userRepository,
                                IActivityRepository activityRepository)
        {
            _effortDefaultRepository = effortDefaultRepository;
            _activityDefaultRepository = activityDefaultRepository;
            _projectRepository = projectRepository;
            _projectConfigRepository = projectConfigRepository;
            _effortRepository = effortRepository;
            _projectActivityRepository = projectActivityRepository;
            _currentEffortRepository = currentEffortRepository;
            _userManager = userManager;
            _projectMemberRepository = projectMemberRepository;
            _userRepository = userRepository;
            _activityRepository = activityRepository;
        }

        [HttpGet]
        public IActionResult Index(string utilizationTimesheet, string projectCode)
        {
            var currentUser = this.User;
            var currentUserId = _userManager.GetUserId(currentUser);

            if (utilizationTimesheet == null || utilizationTimesheet == "")
            {
                utilizationTimesheet = DateTime.Now.ToString("yyyy-MM-dd");
            }

            DailyViewModel dailyViewModel = new DailyViewModel();
            List<string> projectCodes;
            if (currentUser.IsInRole("admin"))
            {
                projectCodes = _projectRepository.GetManyByCreatedBy(currentUserId).OrderByDescending(x => x.CreatedAt).Select(x => x.Id).ToList();
            }
            else
            {
                projectCodes = _projectMemberRepository.GetManyByMemberId(currentUserId, new string[] { "Project" })
                                .Select(x => x.Project).OrderBy(x => x.CreatedAt).Select(x => x.Id).Distinct().ToList();
            }


            if (projectCode == null || projectCode == "")
            {
                projectCode = projectCodes.FirstOrDefault();
            }

            var project = _projectRepository.GetSingleById(projectCode);
            if (project == null)
            {
                return View(null);
            }
            var projectConfig = _projectConfigRepository.GetSingleByProject(project.Id);
            var projectActivity = _projectActivityRepository.GetSingleByConfig(projectConfig.Id);
            var efforts = _effortRepository.GetManyByActivity(projectActivity.Id).ToList();
            List<CurrentEffort> currentEfforts = _currentEffortRepository.GetAllByDate(project.Id, utilizationTimesheet).ToList();
            if (currentEfforts == null || currentEfforts.Count() == 0)
            {
                foreach (var item in efforts)
                {
                    CurrentEffort currentEffort = new CurrentEffort();
                    currentEffort.IsDeleted = false;
                    currentEffort.Project = project;
                    currentEffort.StageName = item.StageName;
                    currentEffort.Effort = item;
                    currentEffort.Date = utilizationTimesheet;
                    currentEffort.EffortNumber = "0";
                    _currentEffortRepository.Add(currentEffort);
                    currentEfforts.Add(currentEffort);
                }
            }

            float totalEffort = 0;
            float totalCurrentEffort = 0;
            foreach (var item in efforts)
            {
                totalEffort += float.Parse(item.EffortNumber);
            }

            var allEfforts = _currentEffortRepository.GetAll(project.Id).ToList();
            List<CurrentEffort> allCurrentEfforts = new List<CurrentEffort>();
            foreach (var item in efforts)
            {
                CurrentEffort currentEffort = new CurrentEffort();
                currentEffort.Id = currentEfforts.Where(x => x.Effort.Id == item.Id).FirstOrDefault().Id;
                currentEffort.IsDeleted = false;
                currentEffort.Project = project;
                currentEffort.StageName = item.StageName.UpperFirstLetter();
                currentEffort.Effort = item;
                currentEffort.Date = utilizationTimesheet;
                float number = 0;
                var thisEfforts = allEfforts.Where(x => x.Effort.Id == item.Id).ToList();
                foreach (var thisEffort in thisEfforts)
                {
                    number += float.Parse(thisEffort.EffortNumber);
                }
                currentEffort.EffortNumber = number.ToString();
                totalCurrentEffort += number;
                allCurrentEfforts.Add(currentEffort);
            }

            dailyViewModel.UtilizationTimesheet = utilizationTimesheet;
            dailyViewModel.PlannedEnd = projectConfig.PlannedEnd;
            dailyViewModel.PlannedStart = projectConfig.PlannedStart;
            dailyViewModel.ProjectCode = project.Id;
            dailyViewModel.Status = projectConfig.Status;
            dailyViewModel.Efforts = efforts;
            dailyViewModel.CurrentEfforts = allCurrentEfforts;


            ViewBag.UtilizationTimesheet = utilizationTimesheet;
            ViewBag.ProjectCodes = projectCodes;
            ViewBag.Project = project;
            ViewBag.ProjectActivity = projectActivity;
            ViewBag.ProjectConfig = projectConfig;
            ViewBag.TotalEffort = totalEffort;
            ViewBag.TotalCurrentEffort = totalCurrentEffort;

            return View(dailyViewModel);
        }

        [HttpPost]
        public IActionResult UpdateCurrentEffort(UpdateCurrentEffortViewModel updateCurrentEffortViewModel)
        {
            var currentUser = this.User;
            var currentUserId = _userManager.GetUserId(currentUser);
            var user = _userRepository.GetSingleById(currentUserId);

            var currentEffort = _currentEffortRepository.GetSingleById(updateCurrentEffortViewModel.Id);
            float number = float.Parse(currentEffort.EffortNumber);
            number += float.Parse(updateCurrentEffortViewModel.EffortNumber);
            currentEffort.EffortNumber = number + "";
            currentEffort.UpdatedBy = user;
            _currentEffortRepository.Update(currentEffort);

            Activity activity = new Activity()
            {
                ApplicationUser = _userRepository.GetSingleById(currentUserId),
                Icon = "fas fa-bicycle",
                CreatedAt = DateTime.Now,
                Content = "Update effort in project",
            };
            _activityRepository.Add(activity);

            return Ok();
        }
    }
}
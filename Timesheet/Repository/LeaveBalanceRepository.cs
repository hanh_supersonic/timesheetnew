﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface ILeaveBalanceRepository
    {
        LeaveBalance GetLeaveBalanceByStaffIDYear(TimiosStaff staff, DateTime year, LeaveBalance newLeaveBalance);

        LeaveBalance GetLeaveBalanceByStaffIDYear(TimiosStaff staff, int year);

        void Add(LeaveBalance leaveBalance);

        IEnumerable<LeaveBalance> GetAll();

        void UpdateLeaveBalance(int balType, int leaveBalID, double taken, double balance, string staffName);

        void UpdateDefaulValue(LeaveBalance leaveBalance);
    }

    public class LeaveBalanceRepository : ILeaveBalanceRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public LeaveBalanceRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(LeaveBalance leaveBalance)
        {
            _applicationDbContext.LeaveBalances.Add(leaveBalance);
            _applicationDbContext.SaveChanges();
        }

        public LeaveBalance create_default_leaveBalance()
        {
            LeaveBalance lb = new LeaveBalance();
            lb.GrantedBF = 0;
            lb.GrantedAL = 10;
            lb.GrantedSL = 0;
            lb.TotalAL = 10;
            lb.TakenAL = 0;
            lb.BalanceAL = 10;
            lb.TotalML = 20;
            lb.TakenML = 0;
            lb.BalanceML = 20;
            lb.TotalMPL = 10;
            lb.TakenMPL = 0;
            lb.BalanceMPL = 10;
            lb.TotalCL = 10;
            lb.TakenCL = 0;
            lb.BalanceCL = 10;
            lb.IsDeleted = false;
            return lb;
        }

        public IEnumerable<LeaveBalance> GetAll()
        {
            return _applicationDbContext.LeaveBalances.Include("Staff").Where(x => x.IsDeleted == false);
        }

        public LeaveBalance GetLeaveBalanceByStaffIDYear(TimiosStaff staff, DateTime year, LeaveBalance newLeaveBalance)
        {
            LeaveBalance result = new LeaveBalance();


            result = _applicationDbContext.LeaveBalances
                        .Where(a => a.Staff.Id.Equals(staff.Id)
                        && a.Year.Year == year.Year
                        && a.IsDeleted == false)
                        .FirstOrDefault();

            if (result == null)//make a new leave balance cause change year edi
            {
                //var newLeaveBalance = create_default_leaveBalance();
                newLeaveBalance.Year = DateTime.Now;
                newLeaveBalance.Staff = staff;

                _applicationDbContext.LeaveBalances.Add(newLeaveBalance);
                _applicationDbContext.SaveChanges();
            }


            return result;
        }

        public LeaveBalance GetLeaveBalanceByStaffIDYear(TimiosStaff staff, int year)
        {
            var result = _applicationDbContext.LeaveBalances
                        .Where(a => a.Staff.Id.Equals(staff.Id)
                        && a.Year.Year == year
                        && a.IsDeleted == false)
                        .FirstOrDefault();
            return result;
        }

        public void UpdateDefaulValue(LeaveBalance leaveBalance)
        {
            LeaveBalance oldLeaveBalance = _applicationDbContext.LeaveBalances.Find(leaveBalance.Id);
            oldLeaveBalance.GrantedAL = leaveBalance.GrantedAL;
            oldLeaveBalance.GrantedBF = leaveBalance.GrantedBF;
            oldLeaveBalance.GrantedSL = leaveBalance.GrantedSL;
            oldLeaveBalance.BalanceAL = leaveBalance.BalanceAL;
            oldLeaveBalance.TotalAL = leaveBalance.TotalAL;
            oldLeaveBalance.BalanceML = leaveBalance.BalanceML;
            oldLeaveBalance.TotalML = leaveBalance.TotalML;
            oldLeaveBalance.TotalMPL = leaveBalance.TotalMPL;
            oldLeaveBalance.BalanceMPL = leaveBalance.BalanceMPL;
            oldLeaveBalance.TotalCL = leaveBalance.TotalCL;
            oldLeaveBalance.BalanceCL = leaveBalance.BalanceCL;
            _applicationDbContext.SaveChanges();
        }

        public void UpdateLeaveBalance(int balType, int leaveBalID, double taken, double balance, string staffName)
        {
            LeaveBalance oldLeaveBal = _applicationDbContext.LeaveBalances.Single(a => a.Id.Equals(leaveBalID));

            if (balType == 1)
            {
                oldLeaveBal.TakenAL = taken;
                oldLeaveBal.BalanceAL = balance;
            }
            else if (balType == 2)
            {
                oldLeaveBal.TakenML = taken;
                oldLeaveBal.BalanceML = balance;
            }
            else if (balType == 3)
            {
                oldLeaveBal.TakenMPL = taken;
                oldLeaveBal.BalanceMPL = balance;
            }
            else if (balType == 4)
            {
                oldLeaveBal.TakenCL = taken;
                oldLeaveBal.BalanceCL = balance;
            }

            oldLeaveBal.LastUpdatedOn = DateTime.Now;
            oldLeaveBal.LastUpdatedBy = staffName;

            _applicationDbContext.SaveChanges();

        }
    }
}

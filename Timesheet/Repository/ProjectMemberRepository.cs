﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IProjectMemberRepository
    {
        void Add(ProjectMember projectMember);

        bool IsIn(string projectId, string memberId, int positionId);

        void Delete(string projectId, string memberId, int positionId);

        IEnumerable<ProjectMember> GetManyByProjectId(string projectId, string[] includes);

        IEnumerable<ProjectMember> GetManyByMemberId(string memberId, string[] includes);
    }

    public class ProjectMemberRepository : IProjectMemberRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public ProjectMemberRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(ProjectMember projectMember)
        {
            _applicationDbContext.ProjectMembers.Add(projectMember);
            _applicationDbContext.SaveChanges();
        }

        public void Delete(string projectId, string memberId, int positionId)
        {
            var projectMember = _applicationDbContext.ProjectMembers.Where(x => x.ApplicationUser.Id == memberId
                                                                           && x.Position.Id == positionId &&
                                                                           x.Project.Id == projectId).FirstOrDefault();
            if (projectMember != null)
            {
                _applicationDbContext.ProjectMembers.Remove(projectMember);
                _applicationDbContext.SaveChanges();
            }
        }

        public IEnumerable<ProjectMember> GetManyByMemberId(string memberId, string[] includes)
        {
            if (includes != null && includes.Count() > 0)
            {
                var query = _applicationDbContext.ProjectMembers.Include(includes.First());
                foreach (var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
                return query.Where(x => x.ApplicationUser.Id == memberId);
            }
            return _applicationDbContext.ProjectMembers.Where(x => x.ApplicationUser.Id == memberId);
        }

        public IEnumerable<ProjectMember> GetManyByProjectId(string projectId,string[] includes = null)
        {
            if(includes != null && includes.Count() > 0)
            {
                var query = _applicationDbContext.ProjectMembers.Include(includes.First());
                foreach(var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
                return query.Where(x => x.Project.Id == projectId);
            }
            return _applicationDbContext.ProjectMembers.Where(x => x.Project.Id == projectId);
        }

        public bool IsIn(string projectId, string memberId, int positionId)
        {
            var projectMember = _applicationDbContext.ProjectMembers.Where(x => x.ApplicationUser.Id == memberId
                                                                            && x.Position.Id == positionId &&
                                                                            x.Project.Id == projectId).FirstOrDefault();
            if (projectMember != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

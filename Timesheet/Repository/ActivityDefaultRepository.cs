﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IActivityDefaultRepository
    {
        IEnumerable<ActivityDefault> GetAll();

        ActivityDefault GetSingleById(int Id);
    }

    public class ActivityDefaultRepository : IActivityDefaultRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public ActivityDefaultRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IEnumerable<ActivityDefault> GetAll()
        {
            return _applicationDbContext.ActivityDefaults.ToList();
        }

        public ActivityDefault GetSingleById(int Id)
        {
            return _applicationDbContext.ActivityDefaults.Find(Id);
        }
    }
}

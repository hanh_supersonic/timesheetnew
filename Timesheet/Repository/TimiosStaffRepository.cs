﻿using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNet.Identity;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface ITimiosStaffRepository
    {
        List<TimiosStaff> GetStaffList();

        TimiosStaff GetByUserId(string id);

        IEnumerable<TimiosStaff> GetAllActive();
        IEnumerable<TimiosStaff> GetAll();

        void UpdateInformation(TimiosStaff timiosStaff);

        void LockByUserId(string id);

        void Add(TimiosStaff timiosStaff);

        TimiosStaff GetStaffByEmail(string email);

        void UpdateDesignation(string id, string designation);

        TimiosStaff getstaff_by_id(int? id);

        void create_email(string email, string subject, string body);
    }

    public class TimiosStaffRepository : ITimiosStaffRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public TimiosStaffRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public List<TimiosStaff> GetStaffList()
        {
            List<TimiosStaff> resultList = new List<TimiosStaff>();

            var query = from a in _applicationDbContext.TimiosStaffs
                        where a.Active == true
                        select a;

            resultList = query.ToList();


            return resultList;
        }

        public TimiosStaff GetStaffByEmail(string email)
        {
            TimiosStaff result = new TimiosStaff();

            var query = _applicationDbContext.TimiosStaffs
                .Where(a => a.Email.Equals(email)
                && a.Active == true)
                .FirstOrDefault();
            return query;
        }

        public TimiosStaff getstaff_by_id(int? id)
        {
            TimiosStaff staff = new TimiosStaff();


            staff = _applicationDbContext.TimiosStaffs.Where(s => s.Id == id.ToString()).FirstOrDefault();


            return staff;

        }

        public void create_email(string email, string subject, string body)
        {
            IdentityMessage IM = new IdentityMessage();
            IM.Destination = email;
            IM.Subject = subject;
            IM.Body = body;
            configSendMailKit(IM);
        }

        public static void configSendMailKit(IdentityMessage message)
        {
            var myMessage = new MimeMessage();
            myMessage.From.Add(new MailboxAddress("Admin", "Admin@timios.net"));
            myMessage.To.Add(new MailboxAddress("User", message.Destination));
            myMessage.Sender = (new MailboxAddress("Admin", "Admin@timios.net"));
            myMessage.Subject = message.Subject;
            myMessage.Body = new TextPart("plain") { Text = message.Body };
            myMessage.Cc.Add(new MailboxAddress("mikeyun@timiossolutions.com"));
            myMessage.Cc.Add(new MailboxAddress("apryl@timiossolutions.com"));
            myMessage.Cc.Add(new MailboxAddress("guangyu@timiossolutions.com"));
            myMessage.Cc.Add(new MailboxAddress("ciwei@timiossolutions.com"));
            myMessage.Cc.Add(new MailboxAddress("zane@supersonic.sg"));

            try
            {
                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect("in-v3.mailjet.com", 587, SecureSocketOptions.Auto);

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate("8d179afcd3cdb4c0d8d6314d9bc47fcd", "628a7dcd4f879e58330baa7368475dc3");
                    client.Send(myMessage);
                    var smtp = new SmtpClient(new ProtocolLogger(Console.OpenStandardOutput()));

                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }
        }

        public void Add(TimiosStaff timiosStaff)
        {
            _applicationDbContext.TimiosStaffs.Add(timiosStaff);
            _applicationDbContext.SaveChanges();
        }

        public void LockByUserId(string id)
        {
            var timiosStaff = _applicationDbContext.TimiosStaffs.Where(x => x.ApplicationUser.Id == id).FirstOrDefault();
            timiosStaff.Active = false;
            _applicationDbContext.SaveChanges();
        }

        public TimiosStaff GetByUserId(string id)
        {
            return _applicationDbContext.TimiosStaffs.Where(x => x.ApplicationUser.Id == id).FirstOrDefault();
        }

        public void UpdateInformation(TimiosStaff timiosStaff)
        {
            var oldTimiosStaff = _applicationDbContext.TimiosStaffs.Find(timiosStaff.Id);
            oldTimiosStaff.Contact = timiosStaff.Contact;
            oldTimiosStaff.Fullname = timiosStaff.Fullname;
            oldTimiosStaff.Email = timiosStaff.Email;
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<TimiosStaff> GetAllActive()
        {
            return _applicationDbContext.TimiosStaffs.Where(x => x.Active == true);
        }

        public void UpdateDesignation(string id, string designation)
        {
            TimiosStaff timiosStaff = _applicationDbContext.TimiosStaffs.Find(id);
            timiosStaff.Designation = designation;
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<TimiosStaff> GetAll()
        {
            return _applicationDbContext.TimiosStaffs;
        }
    }
}

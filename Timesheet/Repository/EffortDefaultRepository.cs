﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IEffortDefaultRepository
    {
        IEnumerable<EffortDefault> GetAll();

        EffortDefault GetSingleById(int Id);
    }

    public class EffortDefaultRepository : IEffortDefaultRepository
    {
        readonly ApplicationDbContext _applicationDbContext;
        public EffortDefaultRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IEnumerable<EffortDefault> GetAll()
        {
            return _applicationDbContext.EffortDefaults.ToList();
        }

        public EffortDefault GetSingleById(int Id)
        {
            return _applicationDbContext.EffortDefaults.Find(Id);
        }
    }
}

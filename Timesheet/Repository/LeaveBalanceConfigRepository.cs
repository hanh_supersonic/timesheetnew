﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface ILeaveBalanceConfigRepository
    {
        void Add(LeaveBalanceConfig leaveBalanceConfig);

        IEnumerable<LeaveBalanceConfig> GetByYear(int year);

        void LockByTimiosStaff(string timiosStaffId);

        LeaveBalanceConfig GetByTimiosStaffYear(TimiosStaff timiosStaff, int year);

        LeaveBalanceConfig GetByTimiosStaff(TimiosStaff timiosStaff);

        void Update(LeaveBalanceConfig leaveBalanceConfig);

        LeaveBalanceConfig GetSingleById(int id);

        IEnumerable<LeaveBalanceConfig> GetAll();
    }

    public class LeaveBalanceConfigRepository : ILeaveBalanceConfigRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public LeaveBalanceConfigRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(LeaveBalanceConfig leaveBalanceConfig)
        {
            _applicationDbContext.LeaveBalanceConfigs.Add(leaveBalanceConfig);
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<LeaveBalanceConfig> GetAll()
        {
            return _applicationDbContext.LeaveBalanceConfigs.Include("TimiosStaff").Where(x => x.Active == true);
        }

        public LeaveBalanceConfig GetByTimiosStaff(TimiosStaff timiosStaff)
        {
            return _applicationDbContext.LeaveBalanceConfigs.Where(x => x.TimiosStaff.Id == timiosStaff.Id).FirstOrDefault();
        }

        public LeaveBalanceConfig GetByTimiosStaffYear(TimiosStaff timiosStaff, int year)
        {
            return _applicationDbContext.LeaveBalanceConfigs.Where(x => x.TimiosStaff.Id == timiosStaff.Id && x.Year == year).FirstOrDefault();
        }

        public IEnumerable<LeaveBalanceConfig> GetByYear(int year)
        {
            return _applicationDbContext.LeaveBalanceConfigs.Where(x => x.Year == year);
        }

        public LeaveBalanceConfig GetSingleById(int id)
        {
            return _applicationDbContext.LeaveBalanceConfigs.Include("TimiosStaff").Where(x => x.Id == id).FirstOrDefault();
        }

        public void LockByTimiosStaff(string timiosStaffId)
        {
            var leaveBalanceConfigs = _applicationDbContext.LeaveBalanceConfigs.Where(x => x.TimiosStaff.Id == timiosStaffId).ToList();
            foreach (var leaveBalanceConfig in leaveBalanceConfigs)
            {
                leaveBalanceConfig.Active = false;
                _applicationDbContext.SaveChanges();
            }
        }

        public void Update(LeaveBalanceConfig leaveBalanceConfig)
        {
            LeaveBalanceConfig oldLeaveBalanceConfig = _applicationDbContext.LeaveBalanceConfigs.Find(leaveBalanceConfig.Id);
            oldLeaveBalanceConfig.GrantedAL = leaveBalanceConfig.GrantedAL;
            oldLeaveBalanceConfig.GrantedBF = leaveBalanceConfig.GrantedBF;
            oldLeaveBalanceConfig.GrantedSL = leaveBalanceConfig.GrantedSL;
            oldLeaveBalanceConfig.BalanceAL = leaveBalanceConfig.BalanceAL;
            oldLeaveBalanceConfig.TotalAL = leaveBalanceConfig.TotalAL;
            oldLeaveBalanceConfig.BalanceML = leaveBalanceConfig.BalanceML;
            oldLeaveBalanceConfig.TotalML = leaveBalanceConfig.TotalML;
            oldLeaveBalanceConfig.TotalMPL = leaveBalanceConfig.TotalMPL;
            oldLeaveBalanceConfig.BalanceMPL = leaveBalanceConfig.BalanceMPL;
            oldLeaveBalanceConfig.TotalCL = leaveBalanceConfig.TotalCL;
            oldLeaveBalanceConfig.BalanceCL = leaveBalanceConfig.BalanceCL;
            _applicationDbContext.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IPositionRepository
    {
        Position GetSingleById(int id);

        IEnumerable<Position> GetAll();
    }
    public class PositionRepository : IPositionRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public PositionRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IEnumerable<Position> GetAll()
        {
            return _applicationDbContext.Positions;
        }

        public Position GetSingleById(int id)
        {
            return _applicationDbContext.Positions.Find(id);
        }
    }
}

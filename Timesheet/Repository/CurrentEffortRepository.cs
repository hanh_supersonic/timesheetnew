﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface ICurrentEffortRepository
    {
        IEnumerable<CurrentEffort> GetAll(string projectCode);

        IEnumerable<CurrentEffort> GetAllByDate(string projectCode, string date);

        IEnumerable<CurrentEffort> GetAllByUpdatedBy(string userId);

        void Add(CurrentEffort  currentEffort);

        CurrentEffort GetSingleById(int id);

        void Update(CurrentEffort currentEffort);
    }

    public class CurrentEffortRepository : ICurrentEffortRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public CurrentEffortRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(CurrentEffort currentEffort)
        {
            _applicationDbContext.CurrentEfforts.Add(currentEffort);
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<CurrentEffort> GetAll(string projectCode)
        {
            return _applicationDbContext.CurrentEfforts.Where(x => x.Project.Id == projectCode && x.IsDeleted == false);
        }

        public IEnumerable<CurrentEffort> GetAllByDate(string projectCode, string date)
        {
            return _applicationDbContext.CurrentEfforts.Where(x => x.Project.Id == projectCode && x.IsDeleted == false && x.Date == date);

        }

        public IEnumerable<CurrentEffort> GetAllByUpdatedBy(string userId)
        {
            return _applicationDbContext.CurrentEfforts.Include("Project").Where(x => x.UpdatedBy.Id == userId);
        }

        public CurrentEffort GetSingleById(int id)
        {
            return _applicationDbContext.CurrentEfforts.Find(id);
        }

        public void Update(CurrentEffort currentEffort)
        {
            var effort = _applicationDbContext.CurrentEfforts.Find(currentEffort.Id);
            effort.AssignedMember = currentEffort.AssignedMember;
            effort.EffortNumber = currentEffort.EffortNumber;
            _applicationDbContext.SaveChanges();
        }
    }
}

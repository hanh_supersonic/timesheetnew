﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IRoleRepository
    {
        IdentityRole GetByName(string roleName);

        IEnumerable<IdentityRole> GetAll();

        IEnumerable<IdentityRole> GetByUser(string userId);
    }
    public class RoleRepository : IRoleRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public RoleRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IEnumerable<IdentityRole> GetAll()
        {
            return _applicationDbContext.Roles;
        }

        public IdentityRole GetByName(string roleName)
        {
            return _applicationDbContext.Roles.Where(x => x.Name == roleName).FirstOrDefault();
        }

        public IEnumerable<IdentityRole> GetByUser(string userId)
        {
            List<IdentityRole> identityRoles = new List<IdentityRole>();
            var userRoles = _applicationDbContext.UserRoles.Where(x => x.UserId == userId);
            return _applicationDbContext.Roles.Where(x => userRoles.Any(c => c.RoleId == x.Id));
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IBizValueRepository
    {
        IEnumerable<BizValue> GetAll();

        BizValue GetSingleById(int id);

        void Add(BizValue bizValue);

        void Update(BizValue newBizValue);

        void Delete(int id);

        List<BizValue> GetLeaveTypeList();
    }
    public class BizValueRepository : IBizValueRepository
    {
        readonly ApplicationDbContext _applicationDbContext;
        public BizValueRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(BizValue bizValue)
        {
            _applicationDbContext.BizValues.Add(bizValue);
            _applicationDbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var bizValue = _applicationDbContext.BizValues.Find(id);
            bizValue.IsDeleted = true;
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<BizValue> GetAll()
        {
            return _applicationDbContext.BizValues.Where(x => x.IsDeleted == false);
        }

        public BizValue GetSingleById(int id)
        {
            return _applicationDbContext.BizValues.Find(id);
        }

        public void Update(BizValue newBizValue)
        {
            var oldBizValue = _applicationDbContext.BizValues.Find(newBizValue.Id);
            oldBizValue.LastUpdatedBy = newBizValue.LastUpdatedBy;
            oldBizValue.LastUpdatedOn = newBizValue.LastUpdatedOn;
            oldBizValue.SortOrder = newBizValue.SortOrder;
            oldBizValue.Value = newBizValue.Value;

            _applicationDbContext.SaveChanges();
        }

        public List<BizValue> GetLeaveTypeList()
        {
            List<BizValue> resultList = new List<BizValue>();

            resultList = _applicationDbContext.BizValues
                .Where(a => a.Type.Equals("LT")
                && a.IsDeleted == false)
                .OrderBy(a => a.SortOrder)
                .ToList();


            return resultList;
        }
    }
}

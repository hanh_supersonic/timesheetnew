﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;
using Timesheet.Models;

namespace Timesheet.Repository
{
    public interface ILeaveRequestRepository
    {
        void SyncLeaveApp();

        void SubmittedLeave(List<CalendarNote> all_event);

        List<LeaveRequest> GetLeaveRequestListBy(DateTime? year, int? leaveTypeID, int? staffID, string leaveStatus);

        bool leave_req_dup_check(LeaveModel leave);

        void CreateLeaveRequest(LeaveModel leaveModel);

        LeaveRequest GetLeaveRequestByID(int leaveRequestID);

        void UpdateLeaveBalance(int balType, int leaveBalID, double taken, double balance, string staffName);

        void UpdateLeaveRequest(LeaveModel newLeaveModel, bool isReview);
    }

    public class LeaveRequestRepository : ILeaveRequestRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public LeaveRequestRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void SyncLeaveApp()
        {
            //var res = from a in _applicationDbContext.LeaveRequests
            //          join b in _applicationDbContext.TimiosStaffs on
            //          a.TimiosStaff.Id equals b.Id
            //          where a.LeaveStatus == "Approved" && b.Active == true
            //          select new { a.LeaveTypeId, b.Atd_Device, b.Fullname, a.StartDate, a.EndDate, a.EndAM, a.StartAM, a.StartPM, a.EndPM };
            var res = _applicationDbContext.LeaveRequests.Include("TimiosStaff").Where(x => x.LeaveStatus == "Approved" && x.TimiosStaff.Active == true)
                            .Select(x => new { x.LeaveTypeId, x.TimiosStaff.Atd_Device, x.TimiosStaff.Fullname, x.StartDate, x.EndDate, x.EndAM, x.StartAM, x.StartPM, x.EndPM });
            var query = res.ToList();



            for (int i = 0; query.Count > i; i++)
            {
                for (DateTime start = query[i].StartDate.Date; query[i].EndDate.Date >= start; start = start.AddDays(1.0))
                {
                    //no device registereed
                    if (ISHOLIDAY(start)) { continue; }
                    if (query[i].Atd_Device == null) { continue; }

                    AttendanceReport spec_log = _applicationDbContext.AttendanceReports.Where(s => s.PersonnelId == query[i].Atd_Device && s.Timestamp.Date == start).FirstOrDefault();

                    if (spec_log == null) //which means that date does not exist yet
                    {
                        spec_log = new AttendanceReport();
                        spec_log.Name = query[i].Fullname;
                        spec_log.PersonnelId = Convert.ToInt32(query[i].Atd_Device);
                        spec_log.VerifyType = "None";
                        spec_log.DeviceName = "None";
                        spec_log.Timestamp = new DateTime(start.Year, start.Month, start.Day, 12, 0, 0);
                        spec_log.Status = _applicationDbContext.BizValues.Where(s => s.Id == query[i].LeaveTypeId).Select(s => s.Value).FirstOrDefault();
                        _applicationDbContext.AttendanceReports.Add(spec_log);
                        _applicationDbContext.SaveChanges();
                    }
                    else
                    {
                        if (spec_log.HistStatus != spec_log.Status) spec_log.HistStatus = spec_log.Status;

                        spec_log.Status = _applicationDbContext.BizValues.Where(s => s.Id == query[i].LeaveTypeId).Select(s => s.Value).FirstOrDefault();
                        spec_log.Timestamp = new DateTime(start.Year, start.Month, start.Day, 12, 0, 0);


                    }

                    if (spec_log.HistStatus == spec_log.Status)
                    {
                        spec_log.HistStatus = "";
                    }


                    if (query[i].StartAM)
                    {
                        spec_log.Remark = "Half Day Leave in the morning";
                    }

                    if (query[i].StartPM)
                    {
                        spec_log.Remark = "Half Day Leave in the afternoon";
                    }

                    if (query[i].EndAM)
                    {
                        spec_log.Remark = "Half Day Leave in the morning";
                    }

                    if (query[i].EndPM)
                    {
                        spec_log.Remark = "Half Day Leave in the morning";
                    }


                    _applicationDbContext.SaveChanges();


                }

                return;
            }
        }

        public static bool ISHOLIDAY(DateTime date)
        {
            //is holiday on Saturday and SUnday
            //may need to add more
            if (DayOfWeek.Saturday == date.DayOfWeek) { return true; }
            if (DayOfWeek.Sunday == date.DayOfWeek) { return true; }

            return false;
        }

        public void SubmittedLeave(List<CalendarNote> all_event)
        {

            List<LeaveRequest> submitted_leave_req = _applicationDbContext.LeaveRequests.Where(s => s.LeaveStatus != "Rejected").ToList();

            foreach (LeaveRequest submit in submitted_leave_req)
            {

                for (DateTime start = submit.StartDate.Date; submit.EndDate.Date >= start; start = start.AddDays(1.0))
                {
                    CalendarNote new_note = new CalendarNote();
                    new_note.Name = submit.StaffName;
                    new_note.Date = start;
                    new_note.Status = submit.LeaveStatus;
                    new_note.SubmitDate = submit.SubmitDate;
                    all_event.Add(new_note);
                }
            }


        }

        public List<LeaveRequest> GetLeaveRequestListBy(DateTime? year, int? leaveTypeID, int? staffID, string leaveStatus)
        {
            List<LeaveRequest> resultList = new List<LeaveRequest>();


            //var query = from a in _applicationDbContext.LeaveRequests
            //            join b in _applicationDbContext.TimiosStaffs on
            //            a.TimiosStaff.Id equals b.Id
            //            where a.IsDeleted == false && b.Active == true
            //            orderby a.SubmitDate descending
            //            select a;
            var query = _applicationDbContext.LeaveRequests.Include("TimiosStaff").Where(x => x.IsDeleted == false && x.TimiosStaff.Active == true).OrderByDescending(x => x.SubmitDate);
            resultList = query.ToList();


            if (year.HasValue)
            {
                resultList = resultList
                    .Where(a => a.SubmitDate >= new DateTime(year.Value.Year, 1, 1)
                    && a.SubmitDate <= new DateTime(year.Value.Year, 12, 31))
                    .ToList();
            }
            if (leaveTypeID.HasValue)
            {
                resultList = resultList
                .Where(a => a.LeaveTypeId.Equals(leaveTypeID))
                .ToList();

            }
            if (staffID.HasValue)
            {
                resultList = resultList
                    .Where(a => a.TimiosStaff.Id == staffID.Value.ToString() )
                    .ToList();
            }
            if (!string.IsNullOrWhiteSpace(leaveStatus))
            {
                resultList = resultList
                    .Where(a => a.LeaveStatus.Equals(leaveStatus))
                    .ToList();
            }

            return resultList;
        }

        public bool leave_req_dup_check(LeaveModel leave)
        {
            List<LeaveRequest> all_req = _applicationDbContext.LeaveRequests.Where(s => s.Id == int.Parse(leave.StaffModel.Id) && s.LeaveStatus != "Rejected").ToList();

            foreach (LeaveRequest req in all_req)
            {
                if (req.StartDate <= leave.LeaveRequestModel.StartDate && req.EndDate >= leave.LeaveRequestModel.EndDate)
                {
                    return false;
                }

                if (req.StartDate >= leave.LeaveRequestModel.StartDate && req.EndDate <= leave.LeaveRequestModel.EndDate)
                {
                    return false;
                }

                if (req.StartDate == leave.LeaveRequestModel.StartDate && req.EndDate == leave.LeaveRequestModel.EndDate)
                {
                    return false;
                }
            }


            return true;
        }

        public void CreateLeaveRequest(LeaveModel leaveModel)
        {
            LeaveRequest leaveRequest = new LeaveRequest();

            //leaveRequest.Id = int.Parse(leaveModel.StaffModel.Id);
            leaveRequest.ManagerInCharge = leaveModel.LeaveRequestModel.ManagerInCharge;
            leaveRequest.LeaveTypeId = leaveModel.LeaveRequestModel.LeaveTypeId;
            leaveRequest.LeaveTypeOthers = leaveModel.LeaveRequestModel.LeaveTypeOthers;
            leaveRequest.NoOfDays = leaveModel.LeaveRequestModel.NoOfDays;
            leaveRequest.Mon1 = leaveModel.LeaveRequestModel.Mon1;
            leaveRequest.Mon2 = leaveModel.LeaveRequestModel.Mon2;
            leaveRequest.Tue1 = leaveModel.LeaveRequestModel.Tue1;
            leaveRequest.Tue2 = leaveModel.LeaveRequestModel.Tue2;
            leaveRequest.Wed1 = leaveModel.LeaveRequestModel.Wed1;
            leaveRequest.Wed2 = leaveModel.LeaveRequestModel.Wed2;
            leaveRequest.Thu1 = leaveModel.LeaveRequestModel.Thu1;
            leaveRequest.Thu2 = leaveModel.LeaveRequestModel.Thu2;
            leaveRequest.Fri1 = leaveModel.LeaveRequestModel.Fri1;
            leaveRequest.Fri2 = leaveModel.LeaveRequestModel.Fri2;
            leaveRequest.StartDate = leaveModel.LeaveRequestModel.StartDate;
            leaveRequest.StartAM = leaveModel.LeaveRequestModel.StartAM;
            leaveRequest.StartPM = leaveModel.LeaveRequestModel.StartPM;
            leaveRequest.EndDate = leaveModel.LeaveRequestModel.EndDate;
            leaveRequest.EndAM = leaveModel.LeaveRequestModel.EndAM;
            leaveRequest.EndPM = leaveModel.LeaveRequestModel.EndPM;
            leaveRequest.SubmitDate = DateTime.Now;
            leaveRequest.SubmitStaff = leaveModel.StaffModel;
            leaveRequest.TimiosStaff = leaveModel.StaffModel;
            leaveRequest.Designation = leaveModel.StaffModel.Designation;
            leaveRequest.StaffName = leaveModel.StaffModel.Fullname;
            leaveRequest.Email = leaveModel.StaffModel.Email;
            leaveRequest.LeaveStatus = "Submitted";
            leaveRequest.LeaveTypeName = leaveModel.LeaveRequestModel.LeaveTypeName;
            leaveRequest.LastUpdatedBy = leaveModel.StaffModel;
            leaveRequest.LastUpdatedOn = DateTime.Now;
            leaveRequest.Remarks = null;
            leaveRequest.IsDeleted = false;

            _applicationDbContext.LeaveRequests.Add(leaveRequest);
            _applicationDbContext.SaveChanges();

        }

        public LeaveRequest GetLeaveRequestByID(int leaveRequestID)
        {
            LeaveRequest result = new LeaveRequest();

            result = _applicationDbContext.LeaveRequests
                .Where(a => a.Id.Equals(leaveRequestID)
                && a.IsDeleted == false)
                .FirstOrDefault();

            return result;
        }

        public void UpdateLeaveRequest(LeaveModel newLeaveModel, bool isReview)
        {

            LeaveRequest oldLeaveReq = _applicationDbContext.LeaveRequests.Single(a => a.Id.Equals(newLeaveModel.LeaveRequestModel.Id));

            if (!isReview)
            {
                oldLeaveReq.ManagerInCharge = newLeaveModel.LeaveRequestModel.ManagerInCharge;
                oldLeaveReq.LeaveTypeId = newLeaveModel.LeaveRequestModel.LeaveTypeId;
                oldLeaveReq.LeaveTypeOthers = newLeaveModel.LeaveRequestModel.LeaveTypeOthers;
                oldLeaveReq.NoOfDays = newLeaveModel.LeaveRequestModel.NoOfDays;
                oldLeaveReq.Mon1 = newLeaveModel.LeaveRequestModel.Mon1;
                oldLeaveReq.Mon2 = newLeaveModel.LeaveRequestModel.Mon2;
                oldLeaveReq.Tue1 = newLeaveModel.LeaveRequestModel.Tue1;
                oldLeaveReq.Tue2 = newLeaveModel.LeaveRequestModel.Tue2;
                oldLeaveReq.Wed1 = newLeaveModel.LeaveRequestModel.Wed1;
                oldLeaveReq.Wed2 = newLeaveModel.LeaveRequestModel.Wed2;
                oldLeaveReq.Thu1 = newLeaveModel.LeaveRequestModel.Thu1;
                oldLeaveReq.Thu2 = newLeaveModel.LeaveRequestModel.Thu2;
                oldLeaveReq.Fri1 = newLeaveModel.LeaveRequestModel.Fri1;
                oldLeaveReq.Fri2 = newLeaveModel.LeaveRequestModel.Fri2;
                oldLeaveReq.StartDate = newLeaveModel.LeaveRequestModel.StartDate;
                oldLeaveReq.StartAM = newLeaveModel.LeaveRequestModel.StartAM;
                oldLeaveReq.StartPM = newLeaveModel.LeaveRequestModel.StartPM;
                oldLeaveReq.EndDate = newLeaveModel.LeaveRequestModel.EndDate;
                oldLeaveReq.EndAM = newLeaveModel.LeaveRequestModel.EndAM;
                oldLeaveReq.EndPM = newLeaveModel.LeaveRequestModel.EndPM;
                oldLeaveReq.SubmitStaff = newLeaveModel.StaffModel;
                oldLeaveReq.TimiosStaff = newLeaveModel.StaffModel;
                oldLeaveReq.Designation = newLeaveModel.StaffModel.Designation;
                oldLeaveReq.StaffName = newLeaveModel.StaffModel.Fullname;
                oldLeaveReq.LeaveTypeName = newLeaveModel.LeaveRequestModel.LeaveTypeName;
                oldLeaveReq.Email = newLeaveModel.StaffModel.Email;
            }
            else
            {
                oldLeaveReq.LeaveTypeId = newLeaveModel.LeaveRequestModel.LeaveTypeId;
                oldLeaveReq.LeaveTypeOthers = newLeaveModel.LeaveRequestModel.LeaveTypeOthers;
                oldLeaveReq.LeaveStatus = newLeaveModel.LeaveRequestModel.LeaveStatus;
                oldLeaveReq.Remarks = newLeaveModel.LeaveRequestModel.Remarks;
                oldLeaveReq.SubmitStaff = newLeaveModel.StaffModel;
                oldLeaveReq.TimiosStaff = newLeaveModel.StaffModel;
                oldLeaveReq.Designation = newLeaveModel.StaffModel.Designation;
                oldLeaveReq.StaffName = newLeaveModel.StaffModel.Fullname;
                oldLeaveReq.LeaveTypeName = newLeaveModel.LeaveRequestModel.LeaveTypeName;
                oldLeaveReq.Email = newLeaveModel.StaffModel.Email;
            }

            oldLeaveReq.LastUpdatedBy = newLeaveModel.StaffModel;
            oldLeaveReq.LastUpdatedOn = DateTime.Now;

            _applicationDbContext.SaveChanges();

        }
        public void UpdateLeaveBalance(int balType, int leaveBalID, double taken, double balance, string staffName)
        {

            LeaveBalance oldLeaveBal = _applicationDbContext.LeaveBalances.Single(a => a.Id.Equals(leaveBalID));

            if (balType == 1)
            {
                oldLeaveBal.TakenAL = taken;
                oldLeaveBal.BalanceAL = balance;
            }
            else if (balType == 2)
            {
                oldLeaveBal.TakenML = taken;
                oldLeaveBal.BalanceML = balance;
            }
            else if (balType == 3)
            {
                oldLeaveBal.TakenMPL = taken;
                oldLeaveBal.BalanceMPL = balance;
            }
            else if (balType == 4)
            {
                oldLeaveBal.TakenCL = taken;
                oldLeaveBal.BalanceCL = balance;
            }

            oldLeaveBal.LastUpdatedOn = DateTime.Now;
            oldLeaveBal.LastUpdatedBy = staffName;

            _applicationDbContext.SaveChanges();

        }
    }
}

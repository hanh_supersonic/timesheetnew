﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Repository
{
    public class BizValueViewModel
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }
    
        public int SortOrder { get; set; }
    }
}

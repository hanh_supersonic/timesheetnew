﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IActivityRepository
    {
        IEnumerable<Activity> GetByDate(DateTime dateTime);

        void Add(Activity activity);
    }

    public class ActivityRepository : IActivityRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public ActivityRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(Activity activity)
        {
            _applicationDbContext.Activities.Add(activity);
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<Activity> GetByDate(DateTime dateTime)
        {
            return _applicationDbContext.Activities.Include("ApplicationUser").Where(x => x.CreatedAt.Day == dateTime.Day
                                                        && x.CreatedAt.Month == dateTime.Month
                                                        && x.CreatedAt.Year == dateTime.Year);
        }
    }
}

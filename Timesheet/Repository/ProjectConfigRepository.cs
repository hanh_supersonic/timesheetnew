﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IProjectConfigRepository
    {
        void Add(ProjectConfig projectConfig);

        ProjectConfig GetSingleByProject(string projectId);

        void Update(ProjectConfig projectConfig);
    }
    public class ProjectConfigRepository : IProjectConfigRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public ProjectConfigRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(ProjectConfig projectConfig)
        {
            _applicationDbContext.Add(projectConfig);
            _applicationDbContext.SaveChanges();
        }

        public ProjectConfig GetSingleByProject(string projectId)
        {
            return _applicationDbContext.ProjectConfigs.Where(x => x.Project.Id == projectId
                                                            && x.IsUsed == true && x.IsDeleted == false).FirstOrDefault();
        }

        public void Update(ProjectConfig projectConfig)
        {
            var oldProjectConfig = _applicationDbContext.ProjectConfigs.Find(projectConfig.Id);
            oldProjectConfig.Status = projectConfig.Status;
            oldProjectConfig.HourUltilized = projectConfig.HourUltilized;
            oldProjectConfig.PlannedStart = projectConfig.PlannedStart;
            oldProjectConfig.PlannedEnd = projectConfig.PlannedEnd;
            _applicationDbContext.SaveChanges();
        }
    }
}

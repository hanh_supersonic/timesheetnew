﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;
using Timesheet.Models;

namespace Timesheet.Repository
{
    public interface IAttendanceReportRepository
    {
        IEnumerable<CalendarNote> GetCalendarNotes();

        AttendanceReport GetSingleById(int id);

        IEnumerable<AttendanceReport> GetAttendanceLog();

        string getname_byemail(string email);

        int query_status(string status, List<AttendanceReport> all_log);

        List<AttendanceReport> name_filter(List<AttendanceReport> all_log, string name);

        void Update(int id, string name, string deviceName, string status, string timestamp, string remark);

        List<string> status_list();

        List<string> getnamelist();
    }

    public class AttendanceReportRepository : IAttendanceReportRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public AttendanceReportRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IEnumerable<CalendarNote> GetCalendarNotes()
        {
            List<CalendarNote> notes = new List<CalendarNote>();

            //List<Attendance_report> all_log = DB.Attendance_reports.Where(s => s.Status != "Check-In" && s.Status != "Check-Out" && s.Status != "Overtime-In" && s.Status != "Overtime-Out" && s.Status != "Absent" && s.Status != "None").ToList(); 
            var result = from s in _applicationDbContext.AttendanceReports
                         join b in _applicationDbContext.TimiosStaffs on
                         s.PersonnelId equals b.Atd_Device
                         where s.Status != "Check-In" && s.Status != "Check-Out" && s.Status != "Overtime-In" && s.Status != "Overtime-Out" && s.Status != "Absent" && s.Status != "None" && b.Active == true
                         select s;

            var all_log = result.ToList();

            foreach (var log in all_log)
            {
                CalendarNote single_note = new CalendarNote();
                single_note.Name = log.Name;
                single_note.Date = log.Timestamp.Date;
                single_note.Status = log.Status;
                notes.Add(single_note);

            }

            return notes;
        }


        public string getname_byemail(string email)
        {
            string name = string.Empty;

            name = _applicationDbContext.TimiosStaffs.Where(s => s.Email == email).Select(s => s.Fullname).FirstOrDefault();

            return name;
        }

        public List<AttendanceReport> name_filter(List<AttendanceReport> all_log, string name)
        {
            if (name != "All" && name != "Admin")
            {
                int Pers_ID = get_PID_by_Name(name);
                all_log = all_log.Where(s => s.PersonnelId == Pers_ID).ToList();
            }
            return all_log;
        }

        public List<string> getnamelist()
        {
            List<string> namelist;

            var query = from a in _applicationDbContext.AttendanceReports
                        join b in _applicationDbContext.TimiosStaffs on
                        a.PersonnelId equals b.Atd_Device
                        where b.Active == true
                        select a.Name;

            namelist = query.Distinct().ToList();

            return namelist;
        }

        public List<string> status_list()
        {

            List<string> status = _applicationDbContext.AttendanceReports.Where(s => s.Status != "None").Select(s => s.Status).Distinct().ToList();
            status.Add("All");

            return status;
        }

        public int get_PID_by_Name(string name)
        {

            return _applicationDbContext.AttendanceReports.Where(s => s.Name.Contains(name)).Select(s => s.PersonnelId).FirstOrDefault();


            //return 0;//by default
        }

        public IEnumerable<AttendanceReport> GetAttendanceLog()
        {
            List<AttendanceReport> all_log = new List<AttendanceReport>();

            var query = from a in _applicationDbContext.AttendanceReports
                        join b in _applicationDbContext.TimiosStaffs
                        on a.PersonnelId equals b.Atd_Device
                        where b.Active == true
                        select a;

            all_log = query.ToList();


            return all_log;
        }

        public int query_status(string status, List<AttendanceReport> all_log)
        {

            return all_log.Where(s => s.Status == status).Count();

        }

        public AttendanceReport GetSingleById(int id)
        {
            return _applicationDbContext.AttendanceReports.Find(id);
        }

        public void Update(int id, string name, string deviceName, string status, string timestamp, string remark)
        {
            AttendanceReport update_log = _applicationDbContext.AttendanceReports.Where(a => a.Id == id).First();
            update_log.Name = name;
            update_log.DeviceName = deviceName;
            update_log.HistStatus = update_log.Status;
            update_log.Status = status;

            if (update_log.Status == update_log.HistStatus)
            {
                update_log.HistStatus = "";
            }

            update_log.Timestamp = Convert.ToDateTime(timestamp);
            update_log.Remark = remark;

            _applicationDbContext.SaveChanges();
        }
    }
}

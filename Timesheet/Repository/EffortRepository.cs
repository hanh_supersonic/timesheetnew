﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IEffortRepository
    {
        void Add(Effort effort);

        void Update(Effort effort);

        IEnumerable<Effort> GetManyByActivity(int id);
    }

    public class EffortRepository : IEffortRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public EffortRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(Effort  effort)
        {
            _applicationDbContext.Add(effort);
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<Effort> GetManyByActivity(int id)
        {
            return _applicationDbContext.Efforts.Where(x => x.IsDeleted == false && x.ProjectActivity.Id == id).ToList(); 
        }

        public void Update(Effort effort)
        {
            var oldEffort = _applicationDbContext.Efforts.Find(effort.Id);
            oldEffort.EffortNumber = effort.EffortNumber;
            _applicationDbContext.SaveChanges();
        }
    }
}

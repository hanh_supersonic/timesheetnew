﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IUserRepository
    {
        ApplicationUser GetSingleById(string id);

        void DeleteMember(ApplicationUser applicationUser);

        ApplicationUser GetSingleByUsername(string email);

        void UpdateProfile(ApplicationUser user);

        IEnumerable<ApplicationUser> GetAll();

        IEnumerable<ApplicationUser> GetByRole(string roleId);
    }

    public class UserRepository : IUserRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public UserRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void DeleteMember(ApplicationUser applicationUser)
        {
            var user = _applicationDbContext.Users.Find(applicationUser.Id);
            user.IsDeleted = true;
            user.UserName = GetRandomText();
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<ApplicationUser> GetAll()
        {
            return _applicationDbContext.Users;
        }

        public IEnumerable<ApplicationUser> GetByRole(string roleId)
        {
            List<string> userIds = _applicationDbContext.UserRoles.Where(x => x.RoleId == roleId).Select(x => x.UserId).ToList();
            var users = _applicationDbContext.Users.Where(x => userIds.Any(c => c == x.Id));
            return users;
        }

        public ApplicationUser GetSingleById(string id)
        {
            return _applicationDbContext.Users.Find(id);
        }

        public ApplicationUser GetSingleByUsername(string email)
        {
            return _applicationDbContext.Users.Where(x => x.UserName.ToLower() == email.ToLower()).FirstOrDefault();
        }

        public void UpdateProfile(ApplicationUser user)
        {
            var oldUser = _applicationDbContext.Users.Find(user.Id);
            oldUser.FullName = user.FullName;
            oldUser.PhoneNumber = user.PhoneNumber;
            _applicationDbContext.SaveChanges();
        }

        private string GetRandomText()
        {
            var letters = "0123456789ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghjklmnopqrstuvwxyz!@#$%";
            var password = "";
            for (int i = 0; i < 10; i++)
            {
                password += letters.ElementAt((new Random()).Next(0, letters.Count()));
            }
            return password;
        }
    }
}

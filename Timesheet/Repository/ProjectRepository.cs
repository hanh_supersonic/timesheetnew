﻿using System.Collections.Generic;
using System.Linq;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IProjectRepository
    {
        void Add(Project project);

        public IEnumerable<Project> GetAll();

        public IEnumerable<Project> GetManyByCreatedBy(string userId);

        public Project GetSingleById(string id);

        public void Update(Project project);
    }

    public class ProjectRepository : IProjectRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public ProjectRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(Project project)
        {
            _applicationDbContext.Add(project);
            _applicationDbContext.SaveChanges();
        }

        public IEnumerable<Project> GetAll()
        {
            return _applicationDbContext.Projects.ToList();
        }

        public IEnumerable<Project> GetManyByCreatedBy(string userId)
        {
            return _applicationDbContext.Projects.Where(x => x.CreatedBy.Id == userId);
        }

        public Project GetSingleById(string id)
        {
            return _applicationDbContext.Projects.Find(id);
        }

        public void Update(Project project)
        {
            var oldProject = _applicationDbContext.Projects.Find(project.Id);
            oldProject.Name = project.Name;
            oldProject.OrderNumber = project.OrderNumber;
            oldProject.QuotationNumber = project.QuotationNumber;
            oldProject.Company = project.Company;
            oldProject.Description = project.Description;
            _applicationDbContext.SaveChanges();
        }
    }
}

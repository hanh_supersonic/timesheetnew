﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Timesheet.Data;

namespace Timesheet.Repository
{
    public interface IProjectActivityRepository
    {
        public void Add(ProjectActivity projectActivity);

        public ProjectActivity GetSingleByConfig(int configId);

        public void Update(ProjectActivity projectActivity);
    }

    public class ProjectActivityRepository : IProjectActivityRepository
    {
        readonly ApplicationDbContext _applicationDbContext;

        public ProjectActivityRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(ProjectActivity projectActivity)
        {
            _applicationDbContext.Add(projectActivity);
            _applicationDbContext.SaveChanges();
        }

        public ProjectActivity GetSingleByConfig(int configId)
        {
            return _applicationDbContext.ProjectActivities.Where(x => x.IsDeleted == false && x.ProjectConfig.Id == configId).FirstOrDefault();
        }

        public void Update(ProjectActivity projectActivity)
        {
            var oldProjectActivity = _applicationDbContext.ProjectActivities.Find(projectActivity.Id);
            oldProjectActivity.Name = projectActivity.Name;
            _applicationDbContext.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class AttendanceReport
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int PersonnelId { get; set; }

        public string Name { get; set; }

        public string DeviceName { get; set; }

        public string VerifyType { get; set; }

        public string Status { get; set; }

        public DateTime Timestamp { get; set; }

        public string Remark { get; set; }

        public string HistStatus { get; set; }
    }
}

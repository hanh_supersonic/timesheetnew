﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class LeaveBalanceConfig
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public TimiosStaff TimiosStaff { get; set; }

        public double GrantedSL { get; set; }

        public double GrantedBF { get; set; }
        
        public double GrantedAL { get; set; }

        public double TotalAL { get; set; }

        public double BalanceAL { get; set; }

        public double TotalML { get; set; }

        public double BalanceML { get; set; }

        public double TotalMPL { get; set; }

        public double BalanceMPL { get; set; }

        public double TotalCL { get; set; }

        public double BalanceCL { get; set; }

        public int Year { get; set; }

        public bool Active { get; set; }
    }
}

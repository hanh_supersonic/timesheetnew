﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class Project
    {
        [Key]
        public string Id { get; set; }

        public string Name { get; set; }

        public string Company { get; set; }

        public string Description { get; set; }

        public string QuotationNumber { get; set; }

        public string OrderNumber { get; set; }

        public string CancelDate { get; set; }

        public ApplicationUser CreatedBy { get; set; }

        public string CreatedAt { get; set; }

        public ApplicationUser UpdatedBy { get; set; }

        public string UpdatedAt { get; set; }
    }
}

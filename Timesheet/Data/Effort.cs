﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class Effort
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public ProjectActivity ProjectActivity { get; set; }

        public string StageName { get; set; }

        public string EffortNumber { get; set; }

        public ApplicationUser CreatedBy { get; set; }

        public string CreatedAt { get; set; }

        public ApplicationUser UpdatedBy { get; set; }

        public ApplicationUser AssignedMember { get; set; }

        public string UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }

        public Project Project { get; set; }
    }
}

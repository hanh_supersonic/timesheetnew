﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class CurrentEffort
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public Project Project { get; set; }

        public Effort Effort { get; set; }

        public string Date { get; set; }

        public string StageName { get; set; }

        public ApplicationUser UpdatedBy { get; set; }

        public ApplicationUser AssignedMember { get; set; }

        public string EffortNumber { get; set; }

        public bool IsDeleted { get; set; }

    }
}

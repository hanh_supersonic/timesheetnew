﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class ProjectConfig
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public Project Project { get; set; }

        public string HourUltilized { get; set; }

        public string CreatedDate { get; set; }

        public bool IsUsed { get; set; }

        public int Status { get; set; }

        public string PlannedStart { get; set; }

        public string PlannedEnd { get; set; }

        public bool IsDeleted { get; set; }
    }
}

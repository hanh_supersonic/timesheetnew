﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Timesheet.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectConfig> ProjectConfigs { get; set; }
        public DbSet<ProjectActivity> ProjectActivities { get; set; }
        public DbSet<Effort> Efforts { get; set; }
        public DbSet<CurrentEffort> CurrentEfforts { get; set; }
        public DbSet<ActivityDefault> ActivityDefaults { get; set; }
        public DbSet<EffortDefault> EffortDefaults { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<ProjectMember> ProjectMembers { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<BizValue> BizValues { get; set; }
        public DbSet<LeaveBalance> LeaveBalances { get; set; }
        public DbSet<LeaveRequest> LeaveRequests { get; set; }
        public DbSet<TimiosStaff> TimiosStaffs { get; set; }
        public DbSet<AttendanceReport> AttendanceReports { get; set; }
        public DbSet<LeaveBalanceConfig> LeaveBalanceConfigs { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class Header
    {
        public int Id { get; set; }

        public string Icon { get; set; }

        public string Name { get; set; }

        public int ParentId { get; set; }

        public bool IsShown { get; set; }

        public string Link { get; set; }

        public string Active { get; set; }

        public bool DoesHaveChild { get; set; }

        public int Order { get; set; }

        public List<string> RoleNames { get; set; }
    }
}

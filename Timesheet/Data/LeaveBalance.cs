﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class LeaveBalance
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public DateTime Year { get; set; }

        public TimiosStaff Staff { get; set; }

        public double GrantedBF { get; set; }

        public double GrantedAL { get; set; }

        public double GrantedSL { get; set; }

        public double TotalAL { get; set; }

        public double TakenAL { get; set; }

        public double BalanceAL { get; set; }

        public double TotalML { get; set; }

        public double TakenML { get; set; }

        public double BalanceML { get; set; }

        public double TotalMPL { get; set; }
        
        public double TakenMPL { get; set; }

        public double BalanceMPL { get; set; }
        
        public double TotalCL { get; set; }

        public double TakenCL { get; set; }

        public double BalanceCL { get; set; }

        public string LastUpdatedBy { get; set; }

        public DateTime LastUpdatedOn { get; set; }

        public bool IsDeleted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class LeaveRequest
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public TimiosStaff TimiosStaff { get; set; }

        public int ManagerInCharge { get; set; }

        public int LeaveTypeId { get; set; }

        public string LeaveTypeOthers { get; set; }

        public double NoOfDays { get; set; }

        public bool Mon1 { get; set; }

        public bool Mon2 { get; set; }

        public bool Tue1 { get; set; }

        public bool Tue2 { get; set; }

        public bool Wed1 { get; set; }

        public bool Wed2 { get; set; }

        public bool Thu1 { get; set; }

        public bool Thu2 { get; set; }

        public bool Fri1 { get; set; }

        public bool Fri2 { get; set; }

        public DateTime StartDate { get; set; }

        public bool StartAM { get; set; }

        public bool StartPM { get; set; }

        public DateTime EndDate { get; set; }

        public bool EndAM { get; set; }

        public bool EndPM { get; set; }

        public DateTime SubmitDate { get; set; }

        public TimiosStaff SubmitStaff { get; set; }

        public string LeaveStatus { get; set; }

        public TimiosStaff LastUpdatedBy { get; set; }

        public DateTime LastUpdatedOn { get; set; }

        public string Remarks { get; set; }

        public bool IsDeleted { get; set; }

        public string LeaveTypeName { get; set; }

        public string StaffName { get; set; }

        public string Designation {get; set; }

        public string Email { get; set; }
    }
}

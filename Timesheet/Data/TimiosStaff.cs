﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class TimiosStaff
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public string Fullname { get; set; }

        public string Designation { get; set; }

        public bool Active { get; set; }

        public int Atd_Device { get; set; }

        public string DevicePass { get; set; }

        public ApplicationUser TeamLeader { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
    }
}

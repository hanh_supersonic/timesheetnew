﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class BizValue
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public string Type { get; set; }

        public string Value { get; set; }

        public int SortOrder { get; set; }

        public bool IsDeleted { get; set; }

        public ApplicationUser LastUpdatedBy { get; set; }

        public DateTime LastUpdatedOn { get; set; }
    }
}

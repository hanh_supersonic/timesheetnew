﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Timesheet.Data
{
    public class ProjectActivity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public ProjectConfig ProjectConfig { get; set; }

        public int Order { get; set; }

        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public Project Project { get; set; }
    }
}
